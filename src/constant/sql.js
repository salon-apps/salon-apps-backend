
exports.history = function (id) {
    const query = "SELECT " +
        "A.idOrder, " +
        "A.note, " +
        "A.timeOrder, " +
        "A.detailLoc, " +
        "A.rating, " +
        "A.ratingNote, " +
        "B.partnerImage, " +	
        "B.fullName, " +
        "Y.nameCategory, " +
        "Z.idProduct, " +
        "Z.nameProduct, " +
        "D.nameDetailProduct, " +
        "D.priceDetailProduct, " +
        "E.nameAddOn, " +
        "E.priceAddOn, " +
        "F.*, " +
        "G.namaLengkap, " +
        "G.alamatLengkap " +
        "FROM userOrder A " +
        "LEFT JOIN partner B ON B.idPartner = A.idPartner " +
        "LEFT JOIN detailProduct D ON D.idDetailProduct = A.idDetailProduct " +
        "LEFT JOIN addOn E ON E.idAddOn = A.idAddOn " +
        "LEFT JOIN statusOrder F ON F.idStatus = A.idStatus " +
        "LEFT JOIN detailPartner G ON G.idPartner = A.idPartner " +
        "LEFT JOIN product Z ON Z.idProduct = D.idProduct " +
        "LEFT JOIN category Y ON Y.idCategory = Z.idCategory " +
        "WHERE A.idUser = " + id + " " +
        "ORDER BY A.timeOrder ASC"
    return query
}

exports.getOrderAll = function () {
    const query = "SELECT " +
        "A.idOrder, " +
        "A.note, " +
        "A.timeOrder, " +
        "A.detailLoc, " +
        "B.hp, " +
        "B.fullName, " +
        "B.userImage, " +
        "Y.nameCategory, " +
        "Z.idProduct, " +
        "Z.nameProduct, " +
        "D.nameDetailProduct, " +
        "D.priceDetailProduct, " +
        "E.nameAddOn, " +
        "E.priceAddOn, " +
        "F.* " +
        "FROM userOrder A " +
        "LEFT JOIN user B ON B.idUser = A.idUser " +
        "LEFT JOIN detailProduct D ON D.idDetailProduct = A.idDetailProduct " +
        "LEFT JOIN addOn E ON E.idAddOn = A.idAddOn " +
        "LEFT JOIN statusOrder F ON F.idStatus = A.idStatus " +
        "LEFT JOIN product Z ON Z.idProduct = D.idProduct " +
        "LEFT JOIN category Y ON Y.idCategory = Z.idCategory " +
        "WHERE A.idStatus = 1 AND A.que = 1 " +
        "ORDER BY A.timeOrder ASC"
    return query
}

exports.getHistoryOwner = function (id) {
    const query = "SELECT " +
        "A.idOrder, " +
        "A.note, " +
        "A.timeOrder, " +
        "A.detailLoc, " +
        "B.hp, " +
        "B.fullName, " +
        "Y.nameCategory, " +
        "Z.idProduct, " +
        "Z.nameProduct, " +
        "D.nameDetailProduct, " +
        "D.priceDetailProduct, " +
        "E.nameAddOn, " +
        "E.priceAddOn, " +
        "F.* " +
        "FROM userOrder A " +
        "LEFT JOIN user B ON B.idUser = A.idUser " +
        "LEFT JOIN detailProduct D ON D.idDetailProduct = A.idDetailProduct " +
        "LEFT JOIN addOn E ON E.idAddOn = A.idAddOn " +
        "LEFT JOIN statusOrder F ON F.idStatus = A.idStatus " +
        "LEFT JOIN product Z ON Z.idProduct = D.idProduct " +
        "LEFT JOIN category Y ON Y.idCategory = Z.idCategory " +
        "WHERE A.idPartner = " + id +
        " ORDER BY A.timeOrder ASC"
    return query
}

exports.getOrderByID = function (id) {
    const query = "SELECT " +
        "A.idOrder, " +
        "A.note, " +
        "A.timeOrder, " +
        "A.detailLoc, " +
        "B.hp, " +
        "B.fullName, " +
        "B.userImage, " +
        "Y.nameCategory, " +
        "Z.idProduct, " +
        "Z.nameProduct, " +
        "D.nameDetailProduct, " +
        "D.priceDetailProduct, " +
        "E.nameAddOn, " +
        "E.priceAddOn, " +
        "F.* " +
        "FROM userOrder A " +
        "LEFT JOIN user B ON B.idUser = A.idUser " +
        "LEFT JOIN detailProduct D ON D.idDetailProduct = A.idDetailProduct " +
        "LEFT JOIN addOn E ON E.idAddOn = A.idAddOn " +
        "LEFT JOIN statusOrder F ON F.idStatus = A.idStatus " +
        "LEFT JOIN product Z ON Z.idProduct = D.idProduct " +
        "LEFT JOIN category Y ON Y.idCategory = Z.idCategory " +
        "WHERE A.idOrder = " + id;
    return query
}

exports.updateStatus = function () {
    const query = "SELECT B.firebaseToken, A.* FROM userOrder A LEFT JOIN token B on B.idUser = A.idUser ";
    return query
}

exports.updateStatusFinish = function () {
    const query = "SELECT B.firebaseToken,E.saldo, A.*, C.priceDetailProduct, D.priceAddOn FROM userOrder A LEFT JOIN token B on B.idUser = A.idUser LEFT JOIN detailProduct C ON C.idDetailProduct = A.idDetailProduct LEFT JOIN addOn D ON D.idAddOn = A.idAddOn LEFT JOIN partner E ON E.idPartner = A.idPartner ";
    return query
}