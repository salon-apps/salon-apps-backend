require('dotenv').config();
const ENV = process.env.NODE_ENV || 'development'
const config = require('./db.json')
const configSuperkeren = require('./dbSuperkeren.json')
const errorCode = require('./error.json')
const key = require('./key.json')

exports.DB = function () {
    const data = config[ENV].DB
    return data
}

exports.DBSUPERKEREN = function () {
    const data = configSuperkeren[ENV].DB
    return data
}

exports.key = function (){
    const data = key[ENV]
    return data
}

exports.errorMessage = function (key) {
    const data = errorCode[key]
    if (data) {
        console.log(data)
        return data
    } else {
        return errorCode.internalError
    }
}