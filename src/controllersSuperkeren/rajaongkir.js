require("dotenv").config();
const response = require("../helper");
const con = require("../db/superkeren");
const RajaOngkir = require("node-rajaongkir").Pro(
  "8aad8b0611f7a38538e6dfc5ef0d6eb8"
);

const origin = 2091; // alamat gudang hardcode subdistrict raja ongkir
const courier = [
  'jne',
  'ninja',
  'sicepat',
  'jnt',
  'lion'
]

exports.myCost = async function (req, res) {
  try {
    if (req.body.destination && req.body.weight) {
      let result = []; 
      for (let row of courier){
        let execute = await RajaOngkir.getCosts({
          origin, 
          originType: "subdistrict",
          destination: req.body.destination,
          destinationType: "subdistrict",
          weight: req.body.weight,
          courier: row,
        })
  
        if (execute){
          result = [...result, ...execute.rajaongkir.results]
        }
      }
      response.succes2(res, result);
    } else {
      response.failed2(res, "", "badRequest");
    }
  } catch (err) {
    response.failed2(res, err);
  }
};

exports.checkResi = async function (req, res) {
  try {
    if (!req.body.waybill || !req.body.courier) {
      return response.failed2(res, "", "badRequest");
    } 
    let execute = await RajaOngkir.getJNEWaybill({
      waybill: req.body.waybill,
      courier: req.body.courier
    })
    if (execute){
      response.succes2(res, execute);
    }
  } catch (err) {
    response.failed2(res, err);
  }
};
