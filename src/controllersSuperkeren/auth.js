require("dotenv").config();
const response = require("../helper");
const con = require("../db/superkeren");
const AES = require("../helper/AES");
const { waText } = require("../controllersSuperkeren/whatsapp");

exports.login = async function (req, res) {
  try {
    if (req.body.email && req.body.password) {
        let execute = await con.db(`SELECT * FROM user WHERE email = '${req.body.email}'`);
        if (execute && execute.length > 0) {
            if (AES.decripter(execute[0].password) === req.body.password){
                response.succes2(res, AES.jwtSignOrigin(execute[0]));
            } else {
                response.failed2(res, "", "wrongPass");
            }
        } else {
            response.failed2(res, "", "unregistered");
        }
      } else {
        response.failed2(res, "", "badRequest");
      }
  } catch (err) {
    response.failed2(res, err);
  }
};

exports.register = async function (req, res) {
  try {
    if (req.body.email && req.body.password && req.body.wa) {
      let sql = `INSERT INTO user (id_user, email, password, no_whatsapp, foto_profil) VALUES (NULL, '${req.body.email}', '${AES.encripter(req.body.password)}', '${req.body.wa}', NULL);`;
      let execute = await con.db(sql);
      if (execute.insertId) {
        let getToken = await con.db(`SELECT * FROM user WHERE id_user = ${execute.insertId}`);
        if (getToken && getToken.length > 0){
            response.succes2(res, AES.jwtSignOrigin(getToken[0]));
        }
      }
    } else {
      response.failed2(res, "", "badRequest");
    }
  } catch (err) {
    console.log(err);
    response.failed2(res, err);
  }
};

exports.changePass = async function (req, res) {
  try {
    if (req.body.password && req.body.email) {
      let sql = `UPDATE user SET password = '${req.body.password}' WHERE email = '${req.body.email}'`;
      let execute = await con.db(sql);
      if (execute) {
        response.succes2(res, execute);
      }
    } else {
      response.failed2(res, "", "badRequest");
    }
  } catch (err) {
    console.log(err);
    response.failed2(res, err);
  }
};

exports.otp = async function (req, res) {
  try {
    if (req.body.wa) {
      let otp = Math.floor(1000 + Math.random() * 9000);
      if(req.body.otp){
        otp = req.body.otp;
      }
      let message = `
      *SUPERKEREN*

      Kode OTP Anda Adalah *${otp}*
      harap tidak memberikan kode otp kesiapapun
      `;
      waText({number: req.body.wa, message})
      response.succes2(res, "sip");
    } else {
      response.failed2(res, "", "badRequest");
    }
  } catch (err) {
    console.log(err);
    response.failed2(res, err);
  }
};
