require("dotenv").config();
const response = require("../helper");
const con = require("../db/superkeren");
const AES = require("../helper/AES");
const listPaymentContant = require('../constant/manualPayment.json');

exports.listPayment = async function (_req, res) {
  try {
    response.succes2(res, listPaymentContant);
  } catch (err) {
    response.failed2(res, err);
  }
};

exports.getPayment = async function (req, res) {
  try {
    let getObj = listPaymentContant.filter(function (rows) {
      return rows.code === req.params.id;
    });
    response.succes2(res, getObj[0]);
  } catch (err) {
    response.failed2(res, err);
  }
};
