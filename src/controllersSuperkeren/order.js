require("dotenv").config();
const response = require("../helper");
const con = require("../db/superkeren");
const AES = require("../helper/AES");
const listPayment = require("../constant/manualPayment.json");
const { waText } = require("../controllersSuperkeren/whatsapp");
const { formatNumber } = require("../helper/formatNumber");

const followUpTransactions = async ({id_user, id_produk, id_alamat, jumlah, total_harga, total_pengiriman, kode_unix, total_pembayaran, pembayaran_code}) => {
  try {
    let user = await con.db(`SELECT * FROM user WHERE id_user = ${id_user}`);
    let product = await con.db(`SELECT * FROM produk WHERE id_produk = ${id_produk}`);
    let address = await con.db(`SELECT * FROM alamat WHERE id_alamat = ${id_alamat}`);
    let payment = listPayment.filter(function (rows) {
      return rows.code === pembayaran_code;
    });
    let message = `
    Hallo kak  ${address[0].nama_penerima} 

Terima kasih telah mengisi alamat pemesanannya.

Mohon konfirmasinya, Saya akan kirim ${product[0].nama_product} ke alamat kaka dengan data pengiriman sebagai berikut :
an. ${address[0].nama_penerima} 
${address[0].alamat_lengkap} ${address[0].kode_pos}
${address[0].kecamatan}
${address[0].kota}
${address[0].provinsi}
No Hp : ${user[0].no_whatsapp}
.
Harga Barang ( ${jumlah} pcs ${formatNumber(total_harga)} ) 
Biaya Kirim : ${formatNumber(total_pengiriman)}
Kode Unix Pembayaran : ${formatNumber(kode_unix)}
Total Pembayaran  : ${formatNumber(total_pembayaran)}

Silahkan transfer ke rekening di bawah ini :

${payment[0].title}
${payment[0].desc}
${payment[0].value}

NB : PESANAN AKAN KAMI PROSES SETELAH ADA BUKTI TRANSFER. TERIMAKASIH. 😊
    `;
    waText({number: user[0].no_whatsapp, message});
  } catch (error) {
    console.log(error);
  }
}

const followUpChangeStatus = async (payload) => {
  try {
    
    let message = `
Hallo kak  ${payload.nama_penerima} 

Pembayaran Telah Kami Terima
Terima kasih telah order ${payload.nama_produk} di superkeren.com

Selanjutnya Saya akan kirim ${payload.jumlah} pcs ${payload.nama_produk} 
ke alamat berikut ini:

${payload.nama_penerima}
${payload.alamat_lengkap} ${payload.kode_pos}
Kecamatan: ${payload.kecamatan}
Kebupaten/Kota: ${payload.kota}
Provinsi : ${payload.provinsi}
No Hp: ${payload.no_whatsapp}

Mohon ditunggu ya kak😊
    `;
    waText({number: payload.no_whatsapp, message});
  } catch (error) {
    console.log(error);
  }
}

const followUpChangeStatusTransactions = (payload) => {
  try {
    if (payload.status_transaksi === 'completed'){
      let message = `
      Hallo kak  ${payload.nama_penerima} 
      
      Terima kasih telah order ${payload.nama_produk} di superkeren.com
      
      Ditunggu orderan selanjutnya ya kak😊
          `;
      waText({number: payload.no_whatsapp, message});
    }
    if (payload.status_transaksi === 'cancelled'){
      let message = `
      Yahh kak  ${payload.nama_penerima} 
      
      Orderan kamu ${payload.nama_produk} di superkeren.com
      Udah batal nih ...

      Ditunggu orderanya ya kak😊
          `;
      waText({number: payload.no_whatsapp, message});
    }
    if (payload.status_transaksi === 'refunded'){
      let message = `
Hallo kak  ${payload.nama_penerima} 

Permintaan Pengembalian Barang Telah Kami Terima
Terima kasih telah order ${payload.nama_produk} di superkeren.com

Selanjutnya kakak tinggal mengirim barangnya ke gudang kami di :
Jalan Aries Mustika 7, 
Perumahan Taman Aries Blok A3 No 24-25, 
Kembangan, Meruya Utara, Jakarta Barat

Lalu Kami akan mengirimkan ulang barang yang baru
yaitu ${payload.jumlah} pcs ${payload.nama_produk} 
ke alamat berikut ini:

${payload.nama_penerima}
${payload.alamat_lengkap} ${payload.kode_pos}
Kecamatan: ${payload.kecamatan}
Kebupaten/Kota: ${payload.kota}
Provinsi : ${payload.provinsi}
No Hp: ${payload.no_whatsapp}

Mohon segera dikirim ya kak barangnya biar kami segera mengirim barang yang baru😊
    `;
      waText({number: payload.no_whatsapp, message});
    }
  } catch (error) {
    console.log(error)
  }
}

const followUpChangeStatusTransactionsResi = (payload) => {
  try {
    let message = `
    Hallo kak  ${payload.nama_penerima} 

    ${payload.jumlah}  pcs ${payload.nama_produk} sudah kami kirimkan ya kak😊
    No resinya :  ${payload.no_resi}
    Mohon dipastikan handphone aktif ya kak. Nanti Kaka akan dihubungi oleh kurir by whatsapp / telp . 
    
    Terima kasih
    `;
      waText({number: payload.no_whatsapp, message});
  } catch (error) {
    console.log(error)
  }
}

exports.listAllOrder = async function (req, res) {
  try {
    async function getProducts (ids){
      return await con.db(`SELECT * FROM produk WHERE id_produk in (${ids})`);
    }
    async function getAddress (id){
      return await con.db(`SELECT A.*, B.email, B.no_whatsapp FROM alamat A LEFT JOIN user B ON A.id_user = B.id_user WHERE A.id_alamat = (${id})`);
    }

    let execute = await con.db('SELECT * FROM transaksi');
    let newData = [];
    for(let row of execute){
      let newRow = {...row};
      let getObj = listPayment.filter(function (rows) {
        return rows.code === row.pembayaran_code;
      });
      newRow.payment = getObj[0];
      newRow.products = await getProducts(row.id_produk);
      newRow.alamat_lengkap = await getAddress(row.id_alamat);
      newData.push(newRow);
    }
    response.succes2(res, newData);
  } catch (err) {
    response.failed2(res, err);
  }
};

exports.myOrder = async function (req, res) {
  try {
    let id_user = req.user.id_user;
    let execute = await con.db(`SELECT * FROM transaksi WHERE id_user = ${id_user}`);
    let newData = [];
    for(let row of execute){
      let newRow = {...row};
      let getObj = listPayment.filter(function (rows) {
        return rows.code === row.pembayaran_code;
      });
      newRow.payment = getObj[0];
      newData.push(newRow);
    }
    response.succes2(res, newData);
  } catch (err) {
    response.failed2(res, err);
  }
};

exports.doOrder = async function (req, res) {
  try {
    let id_user = req.user.id_user;
    let {
      id_produk,
      id_order_online,
      id_alamat,
      jumlah,
      total_berat,
      total_harga,
      total_pengiriman,
      kurir_code,
      kurir_service,
      pembayaran_code,
      biaya_pemesanan,
      kode_unix,
      total_pembayaran,
    } = req.body;
    if (
      id_produk &&
      id_order_online &&
      id_alamat &&
      jumlah &&
      total_berat &&
      total_harga &&
      total_pengiriman &&
      kurir_code &&
      kurir_service &&
      pembayaran_code &&
      biaya_pemesanan.toString() &&
      kode_unix &&
      total_pembayaran
    ) {
      let sql = `INSERT INTO transaksi (id_user, id_produk, id_order_online, id_alamat, jumlah, total_berat, total_harga, total_pengiriman, kurir_code, kurir_service, pembayaran_code, biaya_pemesanan, kode_unix, total_pembayaran) VALUES (${id_user}, '${id_produk}', '${id_order_online}', ${id_alamat}, '${jumlah}', ${total_berat}, ${total_harga}, ${total_pengiriman}, '${kurir_code}', '${kurir_service}', '${pembayaran_code}', ${biaya_pemesanan}, ${kode_unix}, ${total_pembayaran});`;

      let create = await con.db(sql);
      if (create) {
        response.succes2(res, create);
        followUpTransactions({id_user, id_produk, id_alamat, jumlah, total_harga, total_pengiriman, kode_unix, total_pembayaran, pembayaran_code});
      }
    } else {
      response.failed2(res, "", "badRequest");
    }
  } catch (err) {
    console.log(err)
    response.failed2(res, err);
  }
};

exports.updatePayment = async function (req, res) {
  try {
    if (!req.body.id_transaksi || !req.body.status) {
      throw {
        message: "id_transaksi & status required",
        status: 400,
      };
    }
    let execute = await con.db(`UPDATE transaksi SET status_pembayaran = '${req.body.status}', status_transaksi = 'processing' WHERE id_transaksi = ${req.body.id_transaksi}`);
    response.succes2(res, execute);
    if (execute && req.body.status === 'paid'){
      let resp = await con.db(`SELECT A.jumlah, B.email, B.no_whatsapp, C.nama_produk, D.* FROM transaksi A LEFT JOIN user B ON A.id_user = B.id_user LEFT JOIN produk C ON A.id_produk = C.id_produk LEFT JOIN alamat D ON A.id_alamat = D.id_alamat WHERE A.id_transaksi = ${req.body.id_transaksi}`);
      followUpChangeStatus(resp[0]);
    }
  } catch (err) {
    console.log(err)
    response.failed2(res, err);
  }
};

exports.updateTransactionsStatus = async function (req, res) {
  try {
    if (!req.body.id_transaksi || !req.body.status) {
      throw {
        message: "id_transaksi & status required",
        status: 400,
      };
    }
    let execute = await con.db(`UPDATE transaksi SET status_transaksi = '${req.body.status}' WHERE id_transaksi = ${req.body.id_transaksi}`);
    response.succes2(res, execute);
    if (execute){
      let resp = await con.db(`SELECT A.jumlah, A.status_transaksi, B.email, B.no_whatsapp, C.nama_produk, D.* FROM transaksi A LEFT JOIN user B ON A.id_user = B.id_user LEFT JOIN produk C ON A.id_produk = C.id_produk LEFT JOIN alamat D ON A.id_alamat = D.id_alamat WHERE A.id_transaksi = ${req.body.id_transaksi}`);
      followUpChangeStatusTransactions(resp[0]);
    }
  } catch (err) {
    console.log(err)
    response.failed2(res, err);
  }
};

exports.updateTransactionsResi = async function (req, res) {
  try {
    if (!req.body.id_transaksi || !req.body.resi) {
      throw {
        message: "id_transaksi & status required",
        status: 400,
      };
    }
    let execute = await con.db(`UPDATE transaksi SET no_resi = '${req.body.resi}' WHERE id_transaksi = ${req.body.id_transaksi}`);
    response.succes2(res, execute);
    if (execute){
      let resp = await con.db(`SELECT A.jumlah, A.status_transaksi, A.no_resi, B.email, B.no_whatsapp, C.nama_produk, D.* FROM transaksi A LEFT JOIN user B ON A.id_user = B.id_user LEFT JOIN produk C ON A.id_produk = C.id_produk LEFT JOIN alamat D ON A.id_alamat = D.id_alamat WHERE A.id_transaksi = ${req.body.id_transaksi}`);
      followUpChangeStatusTransactionsResi(resp[0]);
    }
  } catch (err) {
    console.log(err)
    response.failed2(res, err);
  }
};