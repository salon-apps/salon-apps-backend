require("dotenv").config();
const WA = require("../constant/whatsapp.json");
const axios  = require("axios");

exports.waText = async function ({
  number, message
}) {
  try {
    if (!WA.deviceID || !number || !message) {
      throw {
        message: "deviceId, number & message required",
        status: 400,
      };
    }

    let payload = {
      headers: {
        "Content-Type": "application/json",
      },
      method: "post",
      url: 'http://207.148.127.64:4114/message-text',
      data: {
        deviceId: WA.deviceID,
        number,
        message
      },
    };
    await axios(payload);
  } catch (err) {
    console.log(err);
  }
};

exports.waImage = async function ({
   number, message, imageUrl
}) {
  try {
    if (!WA.deviceId || !number || !message || !imageUrl) {
      throw {
        message: "deviceId, number, imageUrl & message required",
        status: 400,
      };
    }

    let payload = {
      headers: {
        "Content-Type": "application/json",
      },
      method: "post",
      url: 'http://207.148.127.64:4114/message-text',
      data: {
        deviceId: WA.deviceID,
        number,
        message,
        imageUrl
      },
    };
    await axios(payload);
  } catch (err) {
    console.log(err);
  }
};