require("dotenv").config();
const response = require("../helper");
const con = require("../db/superkeren");
const AES = require("../helper/AES");

exports.listProvince = async function (req, res) {
  try {
    let execute = await con.db("SELECT * FROM provinces");
    response.succes2(res, execute);
  } catch (err) {
    console.log(err);
    response.failed2(res, err);
  }
};

exports.listCities = async function (req, res) {
  try {
    let execute = await con.db(
      `SELECT * FROM cities WHERE province_id = ${req.params.id}`
    );
    response.succes2(res, execute);
  } catch (err) {
    console.log(err);
    response.failed2(res, err);
  }
};

exports.listDistricts = async function (req, res) {
  try {
    let execute = await con.db(
      `SELECT * FROM subdistricts WHERE city_id = ${req.params.id}`
    );
    response.succes2(res, execute);
  } catch (err) {
    console.log(err);
    response.failed2(res, err);
  }
};

exports.searchCitiesByProvince = async function (req, res) {
  try {
    let execute = await con.db(
      `SELECT * FROM cities WHERE province like '%${req.params.name}%'`
    );
    response.succes2(res, execute);
  } catch (err) {
    console.log(err);
    response.failed2(res, err);
  }
};

exports.searchDistrictByCity = async function (req, res) {
  try {
    let execute = await con.db(
      `SELECT * FROM subdistricts WHERE city like '%${req.params.name}%'`
    );
    response.succes2(res, execute);
  } catch (err) {
    console.log(err);
    response.failed2(res, err);
  }
};

exports.getMyAddress = async function (req, res) {
  try {
    let id_user = req.user.id_user;
    let execute = await con.db(
      `SELECT * FROM alamat WHERE id_user = ${id_user}`
    );
    if (execute && execute.length > 0) {
      response.succes2(res, execute[0]);
    } else {
      let create = await con.db(
        `INSERT INTO alamat (id_alamat, id_user, nama_penerima, provinsi, kota, kecamatan, kode_pos, alamat_lengkap) VALUES (NULL, ${id_user}, NULL, NULL, NULL, NULL, NULL, NULL);`
      );
      if (create && create.insertId) {
        execute = await con.db(
          `SELECT * FROM alamat WHERE id_user = ${id_user}`
        );
        response.succes2(res, execute[0]);
      }
    }
  } catch (err) {
    console.log(err);
    response.failed2(res, err);
  }
};

exports.updateAddress = async function (req, res) {
  try {
    let execute = await con.db(
      `UPDATE alamat SET nama_penerima = '${req.body.nama_penerima}', provinsi = '${req.body.provinsi}', kota = '${req.body.kota}', kecamatan = '${req.body.kecamatan}', kode_pos = '${req.body.kode_pos}', alamat_lengkap = '${req.body.alamat_lengkap}' WHERE alamat.id_alamat = ${req.body.id_alamat}`
    );
    response.succes2(res, execute[0]);
  } catch (err) {
    console.log(err);
    response.failed2(res, err);
  }
};

exports.updateAddressOld = async function (req, res) {
  try {
    let execute = await con.db(
      `UPDATE alamat SET nama_penerima = '${req.body.name}' WHERE id_alamat = ${req.body.id_alamat}`
    );
    response.succes2(res, execute);
  } catch (err) {
    console.log(err);
    response.failed2(res, err);
  }
};
