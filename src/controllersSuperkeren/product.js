require("dotenv").config();
const response = require("../helper");
const con = require("../db/superkeren");
const AES = require("../helper/AES");

exports.getProduct = async function (_req, res) {
  try {
    response.succes2(res, await con.db('SELECT * FROM produk'))
  } catch (err) {
    response.failed2(res, err);
  }
};

exports.getProductByID = async function (req, res) {
  try {
    let execute = await con.db(`SELECT * FROM produk WHERE id_produk = ${req.params.id}`);
    response.succes2(res, execute[0]);
  } catch (err) {
    console.log(err);
    response.failed2(res, err);
  }
};
