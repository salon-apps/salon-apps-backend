require('dotenv').config()
const mariadb = require('mariadb');
const config = require('../constant');

const poolDatabase = mariadb.createPool({
    host: config.DBSUPERKEREN().HOST,
    user: config.DBSUPERKEREN().DB_USER,
    password: config.DBSUPERKEREN().DB_PASS,
    database: config.DBSUPERKEREN().DB_NAME,
    connectionLimit: 5,
});

exports.db = async function (query) {
    let conn;
    try {

        conn = await poolDatabase.getConnection();
        const execute = await conn.query(query);
        return execute

    } catch (err) {
        throw err;
    } finally {
        if (conn) conn.release(); //release to pool
    }
}