-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 25, 2020 at 04:32 PM
-- Server version: 8.0.20
-- PHP Version: 7.2.24-0ubuntu0.18.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cantikappdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `addOn`
--

CREATE TABLE `addOn` (
  `idAddOn` int NOT NULL,
  `idProduct` int NOT NULL,
  `idDetailProduct` int NOT NULL,
  `nameAddOn` varchar(50) NOT NULL,
  `priceAddOn` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `addOn`
--

INSERT INTO `addOn` (`idAddOn`, `idProduct`, `idDetailProduct`, `nameAddOn`, `priceAddOn`) VALUES
(1, 1, 5, 'Extra Bleach', 130000),
(2, 1, 6, 'Extra Bleach', 170000),
(3, 1, 7, 'Extra Bleach', 200000),
(4, 7, 14, 'Remove Gel', 30000),
(5, 7, 15, 'Remove Gel', 30000),
(6, 7, 16, 'Remove Gel', 60000),
(7, 8, 17, 'Remove Gel', 30000),
(8, 8, 18, 'Remove Gel', 30000),
(9, 8, 19, 'Remove Gel', 60000),
(10, 9, 20, 'Serum Galvanic Spa', 75000),
(11, 9, 21, 'Serum Galvanic Spa', 75000);

--
-- Triggers `addOn`
--
DELIMITER $$
CREATE TRIGGER `ADDON_CREATE` BEFORE INSERT ON `addOn` FOR EACH ROW UPDATE apiVersion set version = (version + 1)
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `ADDON_UPDATE` BEFORE UPDATE ON `addOn` FOR EACH ROW UPDATE apiVersion set version = (version + 1)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `apiVersion`
--

CREATE TABLE `apiVersion` (
  `idApi` int NOT NULL,
  `version` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `apiVersion`
--

INSERT INTO `apiVersion` (`idApi`, `version`) VALUES
(1, 23);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `idCategory` int NOT NULL,
  `nameCategory` varchar(50) NOT NULL,
  `descCategory` text NOT NULL,
  `imageCategory` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`idCategory`, `nameCategory`, `descCategory`, `imageCategory`) VALUES
(1, 'Hair Treatment', 'Percantik rambut anda dengan berbagai penataan & perawatan rambut', NULL),
(2, 'Nail Treatment', 'Layanan untuk mempercantik kuku kamu dengan nail polish yang banyak pilihan warna atau dengan buffer shining', NULL),
(3, 'Facial & Body Massage', 'Manjakan wajah anda dengan layanan facial dari kami, kamu bisa memilih layanan facial sesuai dengan kebutuhan kulitmu. Dikerjakan oleh therapist yang profesional dan menggunakan produk-produk pilihan', NULL);

--
-- Triggers `category`
--
DELIMITER $$
CREATE TRIGGER `CATEGORY_UPDATE` BEFORE UPDATE ON `category` FOR EACH ROW UPDATE apiVersion set version = (version + 1)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `detailProduct`
--

CREATE TABLE `detailProduct` (
  `idDetailProduct` int NOT NULL,
  `idProduct` int NOT NULL,
  `nameDetailProduct` varchar(50) NOT NULL,
  `priceDetailProduct` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `detailProduct`
--

INSERT INTO `detailProduct` (`idDetailProduct`, `idProduct`, `nameDetailProduct`, `priceDetailProduct`) VALUES
(1, 2, 'Blow Short Hair', 100000),
(2, 2, 'Blow Long Hair', 150000),
(3, 3, 'Short Hair', 100000),
(4, 3, 'Long Hair', 150000),
(5, 1, 'Short Hair', 350000),
(6, 1, 'Medium Hair', 400000),
(7, 1, 'Long Hair', 450000),
(8, 4, 'Short Hair', 130000),
(9, 4, 'Medium Hair', 150000),
(10, 4, 'Long Hair', 180000),
(11, 6, 'Short Hair', 150000),
(12, 6, 'Medium Hair', 170000),
(13, 6, 'Long Hair', 190000),
(14, 7, 'Manicure', 120000),
(15, 7, 'Padicure', 130000),
(16, 7, 'Manicure & Padicure', 200000),
(17, 8, 'Manicure Gel', 230000),
(18, 8, 'Padicure Gel', 250000),
(19, 8, 'Manicure & Padicure Gel', 450000),
(20, 9, 'Face Thitening', 250000),
(21, 9, 'Face Sliming', 250000),
(22, 9, 'Acne with serum galvanic spa', 350000),
(23, 9, 'Rejuvenating with serum galvanic spa', 350000),
(24, 9, 'Anti aging with serum galvanic spa', 400000),
(25, 10, 'Totok wajah 60 Menit', 150000),
(26, 10, 'Massage 90 Menit', 200000),
(27, 10, 'Massage with scrub 90 Menit', 250000);

--
-- Triggers `detailProduct`
--
DELIMITER $$
CREATE TRIGGER `DETAIL_PRODUCT_UPDATE` BEFORE UPDATE ON `detailProduct` FOR EACH ROW UPDATE apiVersion set version = (version + 1)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `idLoc` int NOT NULL,
  `idUser` int DEFAULT NULL,
  `display` varchar(100) DEFAULT NULL,
  `latitude` varchar(50) DEFAULT NULL,
  `longitude` varchar(50) DEFAULT NULL,
  `detailLoc` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`idLoc`, `idUser`, `display`, `latitude`, `longitude`, `detailLoc`) VALUES
(9, 9, 'Pojok, Sukoharjo, Central Java, 57541, Indonesia', '-7.71742917365456', '110.80714188516141', 'Loream epsum dolor');

-- --------------------------------------------------------

--
-- Table structure for table `partner`
--

CREATE TABLE `partner` (
  `idPartner` int NOT NULL,
  `idLoc` int DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `hp` varchar(15) NOT NULL,
  `partnerImage` varchar(255) DEFAULT NULL,
  `fullName` varchar(100) NOT NULL,
  `fingerPrint` varchar(100) DEFAULT NULL,
  `role` enum('owner','partner') NOT NULL DEFAULT 'partner'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `partner`
--

INSERT INTO `partner` (`idPartner`, `idLoc`, `password`, `email`, `hp`, `partnerImage`, `fullName`, `fingerPrint`, `role`) VALUES
(1, NULL, 'U2FsdGVkX18xnZOQ5supGIY/k17KgrnYodegrAonx7A=', '', '081314060004', '', 'cantik homeservice', '', 'owner'),
(2, NULL, 'U2FsdGVkX1+saNanKdc19/4oSGGd4xlX4I2BRD3s0Ug=', 'erikmarlendo@gmail.com', '08999064808', NULL, 'developer', NULL, 'partner');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `idProduct` int NOT NULL,
  `idCategory` int DEFAULT NULL,
  `nameProduct` varchar(100) NOT NULL,
  `descProduct` text NOT NULL,
  `displayPrice` varchar(50) NOT NULL,
  `imageProduct` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`idProduct`, `idCategory`, `nameProduct`, `descProduct`, `displayPrice`, `imageProduct`) VALUES
(1, 1, 'Hair Colloring', 'Pewarnaan rambut anda dengan matrix product kualitas terbaik untuk hasil maksimal', 'Panjang Rambut', NULL),
(2, 1, 'Blow Treatment', 'Service blow dengan hair drayer untuk membuat rambut anda indah, sehat dan bervolume', 'Panjang Rambut', NULL),
(3, 1, 'Catok / Curling', 'Service catok menggunakan alat catok agar membuat rambutmu lebih natural', 'Panjang Rambut', NULL),
(4, 1, 'Creambath Makarizo', 'Jadikan rambut kamu terawat dan sehat serta mencegah kerusakan rambut \r\nAda beberapa variant dan rasakan kelembutan serta keharumannya\r\nCoklat srawbery mint sorbet vanilla green tea', 'Panjang Rambut', NULL),
(6, 1, 'Creambath Scalpcase', 'Scalp care.. formula yg cocok buat mengatasi kerontokan rambut serta yg berketombe.. menjadikan rambut masih tetap segar dlm 72 jam.. sangat cocok buat yg berhijab', 'Panjang Rambut', NULL),
(7, 2, 'Manicure & Padicure (Regular Nail Polish)', 'Layanan untuk mempercantik kuku kamu agar terlihat lebih terawat dilengkapi dengan massage dan scrub. Lebih terawat dan cantik dengan banyak pilihan warna nail polish.', 'Layanan', NULL),
(8, 2, 'Manicure & Padicure Gel Polish', 'Layanan untuk mempercantik kuku kamu agar terlihat lebih terawat dilengkapi dengan massage dan scrub. Lebih terawat dan cantik dengan banyak pilihan warna nail polish.', 'Layanan', NULL),
(9, 3, 'Facial Treatments', 'Layanan khusus untuk percantik wajah dengan rangkaian product pilihan yg membuat kulit wajah kamu lebih kencang glowing dan awet muda.', 'Layanan', NULL),
(10, 3, 'Body Massage', 'Layanan untuk pijat & urut membantu menghilangkan lelah anda.', 'Layanan', NULL);

--
-- Triggers `product`
--
DELIMITER $$
CREATE TRIGGER `PRODUCT_UPDATE` BEFORE UPDATE ON `product` FOR EACH ROW UPDATE apiVersion set version = (version + 1)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `statusOrder`
--

CREATE TABLE `statusOrder` (
  `idStatus` int NOT NULL,
  `nameStatus` varchar(50) NOT NULL,
  `descStatus` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `statusOrder`
--

INSERT INTO `statusOrder` (`idStatus`, `nameStatus`, `descStatus`) VALUES
(1, 'pending', 'status pesanan dimana belum diterima oleh mitra kami'),
(2, 'accepted', 'status pesanan yang udah dikonfirmasi oleh mitra kami dan menunggu sesuai jadwal yang telah ditentukan'),
(3, 'processed', 'sekarang mitra sedang perjalanan kerumah mu'),
(4, 'worked', 'sekarang mitra sedang mengerjakan layanan yang kamu pesan'),
(5, 'finish', 'terimakasih telah menggunakan jasa & layanan cantik home services'),
(6, 'canceled', 'layanan cantik home service telah dibatalkan oleh mu'),
(7, 'rejected', 'layanan cantik home service tidak disetujui oleh mitra, ini terjadi ketika permintaan yang sedang tinggi / conflict waktu pemesanan dengan pelanggan lain');

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `idToken` int NOT NULL,
  `idUser` int DEFAULT NULL,
  `idPartner` int DEFAULT NULL,
  `jwtToken` text,
  `firebaseToken` text,
  `deviceID` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `token`
--

INSERT INTO `token` (`idToken`, `idUser`, `idPartner`, `jwtToken`, `firebaseToken`, `deviceID`) VALUES
(9, 9, NULL, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZFVzZXIiOjksImVtYWlsIjoiZXJpa21hcmxlbmRvQGdtYWlsLmNvbSIsImhwIjpudWxsLCJ1c2VySW1hZ2UiOiJodHRwczovL2xoMy5nb29nbGV1c2VyY29udGVudC5jb20vYS0vQU9oMTRHaDljQm9rcXRzUHVyQ1lZeVFnb1RWYWIzdFR6ZzdkVnFVMWY1TnM9czk2LWMiLCJmdWxsTmFtZSI6IkVyaWNrIE1hcmxlbmRvIiwibG9jYXRpb24iOnsiaWRMb2MiOjksImxhdGl0dWRlIjoiLTcuNzE3NDI5MTczNjU0NTYiLCJsb25naXR1ZGUiOiIxMTAuODA3MTQxODg1MTYxNDEiLCJkaXNwbGF5IjoiUG9qb2ssIFN1a29oYXJqbywgQ2VudHJhbCBKYXZhLCA1NzU0MSwgSW5kb25lc2lhIiwiZGV0YWlsTG9jIjoiTG9yZWFtIGVwc3VtIGRvbG9yIn0sImlhdCI6MTU5MTc1NTk1N30.e-YKN9gcsjZKYHMg73lNMRLnJUjxhhTz_LB_qcjoajU', 'dHlLk7rfeI8:APA91bEqGMjRPLwjqKVg4J_HFW7fjXcpQkzgS0r1qYD7V0820rkE0cWTVnejMt4W1a-PzFa4J8qd4JZF03xrcZp0gZ8fZLZQWxY04TLmmjruvlwW6Bp4Fhj31o6aRwBmT_RMHPPEYyl1', '376e31ddafa1eea9'),
(12, NULL, 1, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZFBhcnRuZXIiOjEsImVtYWlsIjoiIiwiaHAiOiIwODEzMTQwNjAwMDQiLCJwYXJ0bmVySW1hZ2UiOiIiLCJmdWxsTmFtZSI6ImVuZGFuZyIsImlhdCI6MTU5MDM3MDk3M30.VL8q7mW25XS-8xxsXaBK8--MDkGt-Nfi2MrPCY2YlKM', NULL, NULL),
(13, NULL, 2, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZFBhcnRuZXIiOjIsImVtYWlsIjoiZXJpa21hcmxlbmRvQGdtYWlsLmNvbSIsImhwIjoiMDg5OTkwNjQ4MDgiLCJwYXJ0bmVySW1hZ2UiOm51bGwsImZ1bGxOYW1lIjoiZGV2ZWxvcGVyIiwiaWF0IjoxNTkxMTc3NDM5fQ.D8YauhxPKyThijqrL_hfVxbLr2AsxQ0wyaTq2-5xFmE', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `idUser` int NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `hp` varchar(15) DEFAULT NULL,
  `userImage` varchar(255) DEFAULT NULL,
  `fullName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`idUser`, `password`, `email`, `hp`, `userImage`, `fullName`) VALUES
(9, NULL, 'erikmarlendo@gmail.com', NULL, 'https://lh3.googleusercontent.com/a-/AOh14Gh9cBokqtsPurCYYyQgoTVab3tTzg7dVqU1f5Ns=s96-c', 'Erick Marlendo');

-- --------------------------------------------------------

--
-- Table structure for table `userOrder`
--

CREATE TABLE `userOrder` (
  `idOrder` int NOT NULL,
  `idUser` int NOT NULL,
  `idPartner` int DEFAULT NULL,
  `idDetailProduct` int NOT NULL,
  `idAddOn` int DEFAULT NULL,
  `idStatus` int NOT NULL,
  `note` varchar(255) DEFAULT NULL,
  `detailLoc` longtext,
  `timeOrder` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `userOrder`
--

INSERT INTO `userOrder` (`idOrder`, `idUser`, `idPartner`, `idDetailProduct`, `idAddOn`, `idStatus`, `note`, `detailLoc`, `timeOrder`) VALUES
(8, 9, 2, 5, 1, 2, 'Ttttt', '{\"display\":\"Pojok, Sukoharjo, Central Java, 57541, Indonesia\",\"latitude\":-7.71742917365456,\"longitude\":110.80714188516141,\"detailLoc\":\"Loream epsum dolor\"}', '2020-05-22 17:33:02'),
(9, 9, 2, 16, 6, 2, 'Ya gitu deh', '{\"display\":\"Tanjung, Sukoharjo, Central Java, 57541, Indonesia\",\"latitude\":-7.7260474,\"longitude\":110.8209601,\"detailLoc\":\"Loream epsum dolor\"}', '2020-05-24 00:10:33'),
(10, 9, NULL, 4, NULL, 6, 'Yyyyyyyyy', '{\"display\":\"Pojok, Sukoharjo, Central Java, 57541, Indonesia\",\"latitude\":-7.71742917365456,\"longitude\":110.80714188516141,\"detailLoc\":\"Loream epsum dolor\"}', '2020-05-30 13:30:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addOn`
--
ALTER TABLE `addOn`
  ADD PRIMARY KEY (`idAddOn`),
  ADD KEY `FK_ADDON_DETAILPRODUCT` (`idDetailProduct`),
  ADD KEY `FK_ADDON_PRODUCT` (`idProduct`);

--
-- Indexes for table `apiVersion`
--
ALTER TABLE `apiVersion`
  ADD PRIMARY KEY (`idApi`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`idCategory`);

--
-- Indexes for table `detailProduct`
--
ALTER TABLE `detailProduct`
  ADD PRIMARY KEY (`idDetailProduct`),
  ADD KEY `FK_DETAIL_PRODUCT` (`idProduct`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`idLoc`),
  ADD KEY `LOC_USER_LOC_MASTER_FK` (`idUser`);

--
-- Indexes for table `partner`
--
ALTER TABLE `partner`
  ADD PRIMARY KEY (`idPartner`),
  ADD KEY `FK_PARTNER_LOC` (`idLoc`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`idProduct`),
  ADD KEY `FK_PRODUCT_CATEGORY` (`idCategory`);

--
-- Indexes for table `statusOrder`
--
ALTER TABLE `statusOrder`
  ADD PRIMARY KEY (`idStatus`);

--
-- Indexes for table `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`idToken`),
  ADD KEY `F_USER_TOKEN` (`idUser`),
  ADD KEY `F_PARTNER_TOKEN` (`idPartner`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`idUser`);

--
-- Indexes for table `userOrder`
--
ALTER TABLE `userOrder`
  ADD PRIMARY KEY (`idOrder`),
  ADD KEY `FK_USER_ORDER` (`idUser`),
  ADD KEY `FK_DETAILPRODUCT_ORDER` (`idDetailProduct`),
  ADD KEY `FK_IDADDON_PRODUCT` (`idAddOn`),
  ADD KEY `FK_STATUS_ORDER` (`idStatus`),
  ADD KEY `FK_ORDER_PARNER` (`idPartner`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addOn`
--
ALTER TABLE `addOn`
  MODIFY `idAddOn` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `apiVersion`
--
ALTER TABLE `apiVersion`
  MODIFY `idApi` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `idCategory` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `detailProduct`
--
ALTER TABLE `detailProduct`
  MODIFY `idDetailProduct` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `idLoc` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `partner`
--
ALTER TABLE `partner`
  MODIFY `idPartner` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `idProduct` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `statusOrder`
--
ALTER TABLE `statusOrder`
  MODIFY `idStatus` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `token`
--
ALTER TABLE `token`
  MODIFY `idToken` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `idUser` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `userOrder`
--
ALTER TABLE `userOrder`
  MODIFY `idOrder` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `addOn`
--
ALTER TABLE `addOn`
  ADD CONSTRAINT `FK_ADDON_DETAILPRODUCT` FOREIGN KEY (`idDetailProduct`) REFERENCES `detailProduct` (`idDetailProduct`),
  ADD CONSTRAINT `FK_ADDON_PRODUCT` FOREIGN KEY (`idProduct`) REFERENCES `product` (`idProduct`);

--
-- Constraints for table `detailProduct`
--
ALTER TABLE `detailProduct`
  ADD CONSTRAINT `FK_DETAIL_PRODUCT` FOREIGN KEY (`idProduct`) REFERENCES `product` (`idProduct`);

--
-- Constraints for table `location`
--
ALTER TABLE `location`
  ADD CONSTRAINT `LOC_USER_LOC_MASTER_FK` FOREIGN KEY (`idUser`) REFERENCES `user` (`idUser`);

--
-- Constraints for table `partner`
--
ALTER TABLE `partner`
  ADD CONSTRAINT `FK_PARTNER_LOC` FOREIGN KEY (`idLoc`) REFERENCES `location` (`idLoc`);

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `FK_PRODUCT_CATEGORY` FOREIGN KEY (`idCategory`) REFERENCES `category` (`idCategory`);

--
-- Constraints for table `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `F_PARTNER_TOKEN` FOREIGN KEY (`idPartner`) REFERENCES `partner` (`idPartner`),
  ADD CONSTRAINT `F_USER_TOKEN` FOREIGN KEY (`idUser`) REFERENCES `user` (`idUser`);

--
-- Constraints for table `userOrder`
--
ALTER TABLE `userOrder`
  ADD CONSTRAINT `FK_DETAILPRODUCT_ORDER` FOREIGN KEY (`idDetailProduct`) REFERENCES `detailProduct` (`idDetailProduct`),
  ADD CONSTRAINT `FK_IDADDON_PRODUCT` FOREIGN KEY (`idAddOn`) REFERENCES `addOn` (`idAddOn`),
  ADD CONSTRAINT `FK_ORDER_PARNER` FOREIGN KEY (`idPartner`) REFERENCES `partner` (`idPartner`),
  ADD CONSTRAINT `FK_STATUS_ORDER` FOREIGN KEY (`idStatus`) REFERENCES `statusOrder` (`idStatus`),
  ADD CONSTRAINT `FK_USER_ORDER` FOREIGN KEY (`idUser`) REFERENCES `user` (`idUser`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
