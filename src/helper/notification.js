const admin = require("firebase-admin");
const serviceAccount = require("../constant/cantikasalon-e8bbb-firebase-adminsdk-l84a5-f784a737ed.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://cantikasalon-e8bbb.firebaseio.com"
});

exports.fcmSender = async function ({
    fcmToken, notif, data
}) {
    try {
        const notification = await admin.messaging().sendMulticast({
            tokens: [
                fcmToken
            ], // ['token_1', 'token_2', ...]
            notification: {
                title: notif.title ? notif.title : 'Title Notification',
                body: notif.subTitle ? notif.subTitle : 'Body Notification Cantik Home Services',
                imageUrl: 'https://firebasestorage.googleapis.com/v0/b/cantikasalon-e8bbb.appspot.com/o/home%20service.png?alt=media&token=c13048d4-1402-4363-9f99-5e42fd3c8e85',
            },
            data
        });
        if (notification) {
            return notification
        } else {
            return false
        }
    } catch (error) {
        console.log(error)
        return false
    }
}

exports.removeChat = function (idOrder) {
    try {
        let del_ref = admin.database().ref("chat/" + idOrder);
        del_ref.remove()
            .then(function () {
                return true
            })
            .catch(function (error) {
                console.log('Error deleting data:', error);
                return false
            });
    } catch (error) {
        console.log(error)
        return false
    }
}