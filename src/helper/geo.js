function toRad(Value) {
  return Value * Math.PI / 180;
}

function convertLocationIntoRadius(input) {
  const { latAwal, longAwal, latAkhir, longAkhir } = input;
  var R = 6371; // km
  var dLat = toRad(latAkhir - latAwal);
  var dLon = toRad(longAkhir - longAwal);
  var latAwalRad = toRad(latAwal);
  var latAkhirRad = toRad(latAkhir);

  var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(latAwalRad) * Math.cos(latAkhirRad);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c;
  return d.toFixed(2);
}

function compareDistance(a, b) {
  if (a.distance < b.distance) {
    return -1;
  }
  if (a.distance > b.distance) {
    return 1;
  }
  return 0;
}

exports.compareDistance = compareDistance;
exports.convertLocationIntoRadius = convertLocationIntoRadius;
