const CryptoJS = require("crypto-js");
const config = require('../constant');
const jwt = require('jsonwebtoken');
const con = require('../db');

exports.objectEncripter = function (object) {
    try {
        let ciphertext = CryptoJS.AES.encrypt(JSON.stringify(object), config.key().pass);
        return ciphertext.toString();
    } catch (error) {
        console.log(error)
        return 'password encription error'
    }
}

exports.encripter = function (string) {
    try {
        let ciphertext = CryptoJS.AES.encrypt(string, config.key().pass);
        return ciphertext.toString();
    } catch (error) {
        console.log(error)
        return 'password encription error'
    }
}

exports.decripter = function (string) {
    try {
        var bytes = CryptoJS.AES.decrypt(string, config.key().pass);
        return plaintext = bytes.toString(CryptoJS.enc.Utf8);
    } catch (error) {
        console.log(error)
        return 'password decription error'
    }
}

exports.jwtSignOrigin = function (payload) {
    const token = jwt.sign(payload, config.key().jwtKey);
    return token
}

exports.jwtSign = function (payload) {
    const data = {
        idUser: payload.idUser,
        email: payload.email,
        hp: payload.hp,
        userImage: payload.userImage,
        fullName: payload.fullName,
        location: {
            idLoc: payload.idLoc ? payload.idLoc : null,
            latitude: payload.latitude ? payload.latitude : null,
            longitude: payload.longitude ? payload.longitude : null,
            display: payload.display ? payload.display : null,
            detailLoc: payload.detailLoc ? payload.detailLoc : null
        }
    }
    const token = jwt.sign(data, config.key().jwtKey);
    return token
}

exports.jwtSignOwner = function (payload) {
    console.log(payload)
    const data = {
        idPartner: payload.idPartner,
        email: payload.email,
        hp: payload.hp,
        partnerImage: payload.partnerImage,
        fullName: payload.fullName,
        role: payload.role,
        saldo: payload.saldo,
        active: payload.active
    }
    const token = jwt.sign(data, config.key().jwtKey);
    return token
}

exports.pureJWTSign = function (payload) {
    const token = jwt.sign(payload, config.key().jwtKey);
    return token
}

exports.jwtVerify = async function (payload) {
    if (payload.split(" ").length != 2) {
        return {
            error: true,
            data: 'wrong format token'
        }
    } else if (payload.split(" ")[0] != 'beAuty') {
        return {
            error: true,
            data: 'wrong key authentication'
        }
    } else {
        const token = payload.split(" ")[1]
        const verifyToken = await con.db("SELECT * FROM token where jwtToken = '" + token + "'")
        if (verifyToken.length == 0) {
            return {
                error: true,
                data: 'TokenNotFound'
            }
        } else {
            return jwt.verify(token,
                config.key().jwtKey, (err, decode) => {
                    if (err) {
                        return {
                            error: true,
                            data: 'JWT Token Parsed Error'
                        }
                    } else {
                        return {
                            error: false,
                            data: decode
                        }
                    }
                });

        }
    }
}

exports.jwtVerifySuperkeren = async function (payload) {
    if (payload.split(" ").length != 2) {
        return {
            error: true,
            data: 'wrong format token'
        }
    } else if (payload.split(" ")[0] != 'SuPerKeren') {
        return {
            error: true,
            data: 'wrong key authentication'
        }
    } else {
        const token = payload.split(" ")[1]
        if (token && token.length < 300 ) {
            return {
                error: true,
                data: 'TokenNotFound'
            }
        } else {
            return jwt.verify(token,
                config.key().jwtKey, (err, decode) => {
                    if (err) {
                        return {
                            error: true,
                            data: 'JWT Token Parsed Error'
                        }
                    } else {
                        return {
                            error: false,
                            data: decode
                        }
                    }
                });

        }
    }
}

exports.jwtVerifyBearer = async function (payload) {
    if (payload.split(" ").length != 2) {
        return {
            error: true,
            data: 'wrong format token'
        }
    } else if (payload.split(" ")[0] != 'Bearer') {
        return {
            error: true,
            data: 'wrong key authentication'
        }
    } else {
        const token = payload.split(" ")[1];
        console.log(token);
        return jwt.verify(token,
            config.key().jwtKey, (err, decode) => {
                if (err) {
                    return {
                        error: true,
                        data: 'JWT Token Parsed Error'
                    }
                } else {
                    return {
                        error: false,
                        data: decode
                    }
                }
            });

    }
}
