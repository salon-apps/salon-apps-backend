
exports.formatNumber = function (num, cases) {
    if(cases){
        return 'Rp. ' + num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + ' ' + cases;
    }
    return 'Rp. ' + num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  }