const mailjet = require('node-mailjet')
    .connect('addfce34512066d082d710beb3797a5c', 'ce815d03390f45da52d4c8a7bd8e1649')

exports.sender = async function () {

    try {
        const email = await mailjet
            .post("send", { 'version': 'v3.1' })
            .request({
                "Messages": [
                    {
                        "From": {
                            "Email": "erikmarlendo@gmail.com",
                            "Name": "erick"
                        },
                        "To": [
                            {
                                "Email": "erikmarlendo@gmail.com",
                                "Name": "erick"
                            }
                        ],
                        "Subject": "Greetings from Mailjet.",
                        "TextPart": "My first Mailjet email",
                        "HTMLPart": "<h3>Dear passenger 1, welcome to <a href='https://www.mailjet.com/'>Mailjet</a>!</h3><br />May the delivery force be with you!",
                        "CustomID": "AppGettingStartedTest"
                    }
                ]
            })
        return email
    } catch (error) {
        console.log(error)
        return error
    }
}