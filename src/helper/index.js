const errorCode = require('../constant')

exports.failed = function (res, err, code) {
  console.log(err)  
  res.status(200).json({
    error: true,
    errorCode: code,
    message: errorCode.errorMessage(code),
    data: err
  });
}

exports.success = function (res, data) {
  res.status(200).json({
    error: false,
    errorCode: null,
    message: "success",
    data
  });
}

exports.failed2 = function (res, err, code) {  
  res.status(200).json({
    success: false,
    message: errorCode.errorMessage(code),
    error: code,
    payload: err
  });
}

exports.succes2 = function (res, payload) {
  res.status(200).json({
    success: true,
    message: "success",
    error: null,
    payload
  });
}