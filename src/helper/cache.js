const NodeCache = require("node-cache");
const myCache = new NodeCache();

exports.store = function (id, data) {
    try {
        const obj = JSON.stringify(data);
        const save = myCache.set(id, obj, 0);
        if (save) {
            return true
        } else {
            return false
        }
    } catch (error) {
        console.log(error)
        return false;
    }
}

exports.getStore = function (id) {
    try {
        const value = myCache.get(id);
        if (value == undefined) {
            return false
        } else {
            return JSON.parse(value)
        }
    } catch (error) {
        console.log(error)
        return false;
    }
}