const axios = require('axios');
const response = require('../helper');
const AES = require('../helper/AES')

exports.googleAuth = async function (req, res, next) {

    try {
        const token = req.body.tokenID;
        const auth = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=' + token;

        try {
            const googleAuth = await axios.get(auth);
            if (googleAuth.data.name) {
                req.google = googleAuth.data
                next()
            }
        } catch (error) {
            response.failed(res, error.response.data, 'authFailed')
        }
    } catch (ex) {
        response.failed(res, ex)
    }
};

exports.auth = async function (req, res, next) {
    try {
        const token = req.get("Authorization")        
        if (token) {
            const verify = await AES.jwtVerify(token)
            if (verify.error) {
                if (verify.data == 'TokenNotFound') {
                    response.failed(res, verify.data, 'authConflict')
                } else {
                    response.failed(res, verify.data)
                }
            } else {                
                req.user = verify.data
                next()
            }
        } else {
            response.failed(res, '', 'authFailed')
        }
    } catch (error) {
        response.failed(res, error)
    }
}

exports.authSuperkeren = async function (req, res, next) {
    try {
        const token = req.get("Authorization")        
        if (token) {
            const verify = await AES.jwtVerifySuperkeren(token)
            if (verify.error) {
                if (verify.data == 'TokenNotFound') {
                    response.failed2(res, verify.data, 'authConflict')
                } else {
                    response.failed2(res, verify.data)
                }
            } else {                
                req.user = verify.data;
                console.log('verify', verify);
                next()
            }
        } else {
            response.failed2(res, '', 'authFailed')
        }
    } catch (error) {
        response.failed2(res, error)
    }
}