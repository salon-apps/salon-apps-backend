const express = require("express");
const router = express.Router();
const midd = require('../middleware');
const authCtrl = require('../controllersSuperkeren/auth');
const productCtrl = require('../controllersSuperkeren/product');
const addressCtrl = require('../controllersSuperkeren/address');
const rjoCtrl = require('../controllersSuperkeren/rajaongkir');
const paymentCtrl = require('../controllersSuperkeren/payment');
const orederCtrl = require('../controllersSuperkeren/order');


router.get("/", function (_req, res, _next) {
  res.send('Welcome');
});

router.post("/auth/login", authCtrl.login);
router.post("/auth/register", authCtrl.register);
router.post("/auth/change-password", authCtrl.changePass);
router.post("/auth/otp", authCtrl.otp);

router.get("/product", productCtrl.getProduct);
router.get("/product/:id", productCtrl.getProductByID);

router.get("/address", midd.authSuperkeren, addressCtrl.getMyAddress);
router.put("/address", midd.authSuperkeren, addressCtrl.updateAddress);
router.put("/address-old", midd.authSuperkeren, addressCtrl.updateAddressOld);

router.get("/province", midd.authSuperkeren, addressCtrl.listProvince);
router.get("/cities/:id", midd.authSuperkeren, addressCtrl.listCities);
router.get("/cities-by-province/:name", midd.authSuperkeren, addressCtrl.searchCitiesByProvince);
router.get("/district/:id", midd.authSuperkeren, addressCtrl.listDistricts);
router.get("/district-by-city/:name", midd.authSuperkeren, addressCtrl.searchDistrictByCity);

router.get("/payment", midd.authSuperkeren, paymentCtrl.listPayment);
router.get("/payment/:id", midd.authSuperkeren, paymentCtrl.getPayment);
router.put("/payment/update-payment", midd.authSuperkeren, orederCtrl.updatePayment);

router.get("/order", midd.authSuperkeren, orederCtrl.myOrder);
router.post("/order", midd.authSuperkeren, orederCtrl.doOrder);
router.get("/transactions", midd.authSuperkeren, orederCtrl.listAllOrder);
router.put("/transactions/update-status", midd.authSuperkeren, orederCtrl.updateTransactionsStatus);
router.put("/transactions/update-resi", midd.authSuperkeren, orederCtrl.updateTransactionsResi);

router.post("/rjo/cost", midd.authSuperkeren, rjoCtrl.myCost);
router.post("/rjo/check-resi", midd.authSuperkeren, rjoCtrl.checkResi);

module.exports = router;
