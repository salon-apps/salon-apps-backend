const ctrl = require('../controllers');
const productCtrl = require('../controllers/product');
const orderCtrl = require('../controllers/order');
const partnerCtrl = require('../controllers/partner');
const authCtrl = require('../controllers/auth');
const dummyCtrl = require('../controllers/dummy');
const midd = require('../middleware');

module.exports = function (app, upload) {

    const MainRoot = 'salon-apps';

    if (process.env.NODE_ENV !== 'productions') {
        app.use((req, res, next) => {
            const { method, url, query, body } = req;
            console.log('=================================')
            console.log({ method, url, query, body })
            console.log('=================================')
            next()
        })
    }

    app.route(`/${MainRoot}/`).get(ctrl.getAbout)
    app.route(`/${MainRoot}/version`).get(ctrl.version, ctrl.getVersion)
    app.route(`/${MainRoot}/test-encript/:id`).get(ctrl.testEncript)
    app.route(`/${MainRoot}/test-decript`).post(ctrl.testDecript)
    app.route(`/${MainRoot}/test-notif`).get(ctrl.testNotif)
    app.route(`/${MainRoot}/product`).get(ctrl.version, productCtrl.getProduct)
    app.route(`/${MainRoot}/detail-product/:id`).get(productCtrl.getDetailProduct)
    app.route(`/${MainRoot}/update-desc-category`).put(midd.auth, productCtrl.updateDescCategory)
    app.route(`/${MainRoot}/update-desc-product`).put(midd.auth, productCtrl.updateDescProduct)
    app.route(`/${MainRoot}/update-detail-price`).put(midd.auth, productCtrl.updateDetailPrice)
    app.route(`/${MainRoot}/update-addon-price`).put(midd.auth, productCtrl.updateAddOnPrice)
    // app.route(`/${MainRoot}/test-email`).get(ctrl.testEmail)    
    app.route(`/${MainRoot}/google-login-owner`).post(midd.googleAuth, authCtrl.loginGoogleOwner)
    app.route(`/${MainRoot}/detail-register-partner`).post(midd.auth, authCtrl.registerDetailPartner)
    app.route(`/${MainRoot}/detail-partner`).get(midd.auth, authCtrl.detailPartner)
    app.route(`/${MainRoot}/updateloc-partner`).put(midd.auth, authCtrl.updateLocPartner)
    app.route(`/${MainRoot}/ready-partner`).put(midd.auth, authCtrl.updateReady)
    app.route(`/${MainRoot}/login-owner`).post(authCtrl.loginOwner)
    app.route(`/${MainRoot}/login`).post(authCtrl.login)
    app.route(`/${MainRoot}/google-login`).post(midd.googleAuth, authCtrl.loginGoogle)
    app.route(`/${MainRoot}/update-device`).post(midd.auth, authCtrl.updateDevice)
    app.route(`/${MainRoot}/update-device-owner`).post(midd.auth, authCtrl.updateDeviceOwner)
    app.route(`/${MainRoot}/order`).post(midd.auth, productCtrl.order)
    app.route(`/${MainRoot}/get-order/:id`).get(midd.auth, productCtrl.getOrderByID)
    app.route(`/${MainRoot}/reject-order/:id`).put(midd.auth, productCtrl.rejectedOrder)
    app.route(`/${MainRoot}/reject-order-force/:id`).put(midd.auth, productCtrl.rejectedOrderForce)
    app.route(`/${MainRoot}/get-order`).post(midd.auth, orderCtrl.getOrder)
    app.route(`/${MainRoot}/history-owner`).post(midd.auth, orderCtrl.getHistoryOwner)
    app.route(`/${MainRoot}/history`).get(midd.auth, orderCtrl.getHistory)
    app.route(`/${MainRoot}/accept/:id`).put(midd.auth, orderCtrl.accept)
    app.route(`/${MainRoot}/processed/:id`).put(midd.auth, orderCtrl.processed)
    app.route(`/${MainRoot}/worked`).put(midd.auth, orderCtrl.worked)
    app.route(`/${MainRoot}/finish/:id`).put(midd.auth, orderCtrl.finish)
    app.route(`/${MainRoot}/finish-force/:id`).put(midd.auth, orderCtrl.finishForce)
    app.route(`/${MainRoot}/reject/:id`).put(midd.auth, orderCtrl.rejected)
    app.route(`/${MainRoot}/cancel/:id`).put(midd.auth, orderCtrl.canceled)
    app.route(`/${MainRoot}/list-partner`).get(midd.auth, partnerCtrl.getAllPartner)
    app.route(`/${MainRoot}/add-saldo`).post(midd.auth, partnerCtrl.addSaldo)
    app.route(`/${MainRoot}/activate-partner`).put(midd.auth, partnerCtrl.activatePartner)
    app.route(`/${MainRoot}/owner`).get(partnerCtrl.ownerInfo)
    // app.route(`/${MainRoot}/google-register`).post(midd.auth, authCtrl.registerGoogle)

    // admin route
    app.route(`/${MainRoot}/list-user`).get(midd.auth, partnerCtrl.clientAll)
    app.route(`/${MainRoot}/list-order-all`).get(midd.auth, partnerCtrl.clientOrderAll)
    app.route(`/${MainRoot}/detail-partner/:id`).get(midd.auth, partnerCtrl.getDetailPartner)
    app.route(`/${MainRoot}/rating`).put(midd.auth, partnerCtrl.ratingPartner)
    app.route(`/${MainRoot}/upload/partner-image/:id`).post(
        midd.auth, // autentikasi
        upload.single('file'), // sending file to server
        partnerCtrl.uploadFoto, // store filename to database
        partnerCtrl.updateFoto // update filename if file updated and deleted old file
    )
    app.route(`/${MainRoot}/update-detail-partner`).put(midd.auth, partnerCtrl.updateProfilePartner)
    // fake api
    app.route(`/${MainRoot}/dummy-download/:id`).get(dummyCtrl.dummyDownload)
    app.route(`/${MainRoot}/test-api`).get(dummyCtrl.testApi)
    app.route(`/${MainRoot}/sales-performance/:id`).get(dummyCtrl.salesPerformance)

};
