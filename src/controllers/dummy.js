"use strict";
require('dotenv').config()
const response = require('../helper');
const con = require('../db');
const nodeCache = require('../helper/cache');
const errorMessage = require('../constant/error.json');
const notification = require('../helper/notification');
const moment = require('moment');
const SQL = require('../constant/sql');
const { convertLocationIntoRadius } = require('../helper/geo');
const query = require('../constant/sql');
const axios = require('axios');
const jSONMaster1 = require('../assets/dummy/master1.json');
const jSONMaster2 = require('../assets/dummy/master2.json');
const jSONMaster3 = require('../assets/dummy/master3.json');
const jSONMaster4 = require('../assets/dummy/master4.json');

exports.dummyDownload = async function (req, res) {
    try {
        const filePath = `${__dirname}/../assets/master.csv`;
        const fileName = req.params.id + '.csv';
        res.download(filePath, fileName);
    } catch (err) {
        response.failed(res, err)
    }
}

exports.testApi = async function (req, res) {
    try {

        const request = await axios({
            method: 'post',
            url: 'http://116.203.92.59/api/send_message',
            data: {
                phone_no: '628975835238',
                key: 'e03ec7e4af6f52237b09c26fb6fb0e99ff91917f087dbece',
                message: 'test message sinc api from server'
            }
        })

        if (request) {
            console.log(request.data)
            response.success(res, request.data)
        }

    } catch (err) {
        console.log(err)
        response.failed(res, err)
    }
}

exports.salesPerformance = async function (req, res) {
    try {
        let myJson = { name: 'error' };
        const id = Number(req.params.id);
        console.log('ssss', id);
        if (id === 1) {
            myJson = jSONMaster1;
        } else if (id === 2) {
            myJson = jSONMaster2;
        } else if (id === 3) {
            myJson = jSONMaster3;
        } else if (id === 4) {
            myJson = jSONMaster4;
        } else {
            myJson = jSONMaster1;
        }
        response.success(res, myJson);
    } catch (err) {
        response.failed(res, err)
    }
}