require('dotenv').config();
const ENV = process.env.NODE_ENV || 'development';
const PORT = process.env.PORT;
const fs = require('fs');
const response = require('../helper');
const con = require('../db');
const sharp = require('sharp');
const moment = require('moment');

exports.getAllPartner = async function (req, res) {
    try {
        const data = await con.db(`SELECT A.idPartner AS ID, A.email, A.fullName, A.saldo, A.active, A.partnerImage, A.dateJoin, B.* FROM partner A LEFT JOIN detailPartner B ON B.idPartner = A.idPartner where A.role = 'partner' ORDER BY A.active AND A.idPartner ASC`);
        let newData = {
            active: [],
            nonActive: []
        }
        if (data.length > 0) {
            let getActive = data.filter(function (rows) {
                return rows.active == 1
            });

            for (let row of getActive) {
                if (moment(row.dateJoin).isAfter(moment().subtract(1, 'month'))) {
                    row.new = true
                } else {
                    row.new = false
                }
            }

            let getNonActive = data.filter(function (rows) {
                return rows.active == 0
            });

            for (let row of getNonActive) {
                if (moment(row.dateJoin).isAfter(moment().subtract(1, 'month'))) {
                    row.new = true
                } else {
                    row.new = false
                }
            }

            newData.active = getActive;
            newData.nonActive = getNonActive;
        }
        response.success(res, newData);
    } catch (err) {
        response.failed(res, err)
    }
}

exports.getDetailPartner = async function (req, res) {
    try {
        const idOrder = req.params.id;
        const data = await con.db(`SELECT A.idPartner AS ID, A.email, A.fullName, A.saldo, A.active, A.partnerImage, A.dateJoin, B.namaLengkap, B.alamatLengkap, B.hp, C.rating, C.ratingNote FROM userOrder C LEFT JOIN partner A ON C.idPartner = A.idPartner LEFT JOIN detailPartner B ON B.idPartner = A.idPartner LEFT JOIN userOrder D ON D.idPartner = C.idPartner where A.role = 'partner' AND C.idOrder = ${idOrder} ORDER BY A.active AND A.idPartner ASC`);
        response.success(res, data[0]);
    } catch (err) {
        response.failed(res, err)
    }
}

exports.addSaldo = async function (req, res) {
    try {
        const saldo = req.body.saldo;
        const idPartner = req.body.idPartner;

        if (req.user.idPartner === 1) {
            const data = await con.db(`UPDATE partner set saldo = ${saldo} where idPartner = ${idPartner}`);
            response.success(res, data);
        } else {
            response.failed(res, '', 'authWrong')
        }
    } catch (err) {
        response.failed(res, err)
    }
}

exports.activatePartner = async function (req, res) {
    try {
        const idPartner = req.body.idPartner;

        if (req.user.idPartner === 1) {
            const data = await con.db(`UPDATE partner set active = 1 where idPartner = ${idPartner}`);
            response.success(res, data);
        } else {
            response.failed(res, '', 'authWrong')
        }
    } catch (err) {
        response.failed(res, err)
    }
}

exports.ownerInfo = async function (req, res) {
    try {
        const data = await con.db(`SELECT * FROM partner WHERE role = 'owner'`);
        response.success(res, {
            data,
            deviceID: '5781ac156145a235'
        });
    } catch (err) {
        response.failed(res, err)
    }
}

exports.clientAll = async function (req, res) {
    try {
        const data = await con.db(`SELECT idUser, email, hp, userImage, fullName FROM user`);
        response.success(res, data);
    } catch (err) {
        response.failed(res, err)
    }
}

exports.clientOrderAll = async function (req, res) {
    try {
        const data = await con.db(`
        SELECT
            A.idOrder,
            A.note,
            A.timeOrder,
            A.detailLoc,
            B.hp,
            B.fullName,
            B.userImage,
            Y.nameCategory,
            Z.idProduct,
            Z.nameProduct,
            D.nameDetailProduct,
            D.priceDetailProduct,
            E.nameAddOn,
            E.priceAddOn,
            F.*
        FROM
            userOrder A
        LEFT JOIN user B ON
            B.idUser = A.idUser
        LEFT JOIN detailProduct D ON
            D.idDetailProduct = A.idDetailProduct
        LEFT JOIN addOn E ON
            E.idAddOn = A.idAddOn
        LEFT JOIN statusOrder F ON
            F.idStatus = A.idStatus
        LEFT JOIN product Z ON
            Z.idProduct = D.idProduct
        LEFT JOIN category Y ON
            Y.idCategory = Z.idCategory
        ORDER BY
            A.timeOrder ASC
        `);
        response.success(res, data);
    } catch (err) {
        response.failed(res, err)
    }
}

exports.ratingPartner = async function (req, res) {
    try {
        const data = await con.db(`UPDATE userOrder SET rating = ${req.body.rating}, ratingNote = '${req.body.ratingNote}' WHERE idOrder = ${req.body.idOrder}`);
        response.success(res, data);
    } catch (err) {
        response.failed(res, err)
    }
}

exports.uploadFoto = async function (req, res, next) {
    try {
        const data = await con.db(`SELECT * FROM partner WHERE partner.idPartner = ${req.params.id}`);
        if (data.length > 0) {
            let oldURL = data[0].partnerImage.split('/');
            let oldPath = `public/jelita/${oldURL[Number(oldURL.length) - 1]}`;
            fs.unlink(oldPath, (err) => {
                if (err) {
                    console.error(err)
                    return
                }

                //file removed
            })
            let newName = `small_${req.file.filename}`;
            sharp(req.file.path).resize({ width: 400 }).toFile(`./public/jelita/${newName}`, (err, resizeImage) => {
                if (err) {
                    throw err
                } else {
                    fs.unlink(req.file.path, (err) => {
                        if (err) {
                            console.error(err)
                            return
                        }

                        //file removed
                    })
                    let host = 'http://159.65.131.78';
                    if (ENV !== 'production') {
                        host = 'http://localhost'
                    }
                    req.filePath = `${host}:${PORT}/assets/${newName}`;
                    next();
                }
            })
        } else {
            response.failed(res, '')
        }
    } catch (err) {
        response.failed(res, err)
    }
}

exports.updateFoto = async function (req, res) {
    try {
        const update = await con.db(`UPDATE partner SET partnerImage = '${req.filePath}' WHERE partner.idPartner = ${req.params.id}`)
        response.success(res, update);
    } catch (err) {
        response.failed(res, err)
    }
}

exports.updateProfilePartner = async function (req, res) {
    try {
        if (req.body.name.length < 3) {
            response.failed(res, 'Masukan Nama Lengkap')
        } else if (req.body.alamat.length < 3) {
            response.failed(res, 'Masukan Alamat Lengkap')
        } else if (req.body.hp.length < 3) {
            response.failed(res, 'Masukan No')
        } else if (req.body.nik.length < 3) {
            response.failed(res, 'Masukan NIK Dengan Benar')
        } else if (req.body.kk.length < 3) {
            response.failed(res, 'Masukan KK Dengan Benar')
        } else {
            const updateData = await con.db(`UPDATE detailPartner SET namaLengkap = '${req.body.name}', alamatLengkap = '${req.body.alamat}', namaDarurat = '${req.body.darurat}', hubDarurat = '${req.body.hubDar}', alamatDarurat = '${req.body.alamatDar}', hpDarurat = '${req.body.hpDar}', NIK = '${req.body.nik}', KK = '${req.body.kk}', hp = '${req.body.hp}' WHERE detailPartner.idDetailPartner = ${req.body.idDetailPartner}`)
            response.success(res, updateData);
        }
    } catch (err) {
        response.failed(res, err)
    }
}