"use strict";
require('dotenv').config()
const response = require('../helper');
const con = require('../db');
const nodeCache = require('../helper/cache');
const errorMessage = require('../constant/error.json');
const notification = require('../helper/notification');
const moment = require('moment');
const SQL = require('../constant/sql');
const { convertLocationIntoRadius } = require('../helper/geo');
const query = require('../constant/sql');

exports.getProduct = async function (req, res) {
    try {

        const hidePrice = req.query.price === 'false' ? true : false;
        let ID = '';
        if (hidePrice) {
            ID = 'productHidePrice'
        } else {
            ID = 'product'
        }
        const store = nodeCache.getStore(ID)
        let payload = [];

        async function getData() {
            async function addOn(id) {
                let sql = "";
                if (hidePrice) {
                    sql = "SELECT idAddOn, idProduct, idDetailProduct, nameAddOn, priceAddOn = 0 as priceAddOn FROM addOn WHERE idProduct = " + id;
                } else {
                    sql = "SELECT idAddOn, idProduct, idDetailProduct, nameAddOn, priceAddOn FROM addOn WHERE idProduct = " + id;
                }
                return await con.db(sql)
            }

            async function detailProduct(id) {
                let sql = "";
                if (hidePrice) {
                    sql = `SELECT idDetailProduct, nameDetailProduct, priceDetailProduct = 0 as priceDetailProduct FROM detailProduct WHERE idProduct = ${id} ORDER BY priority ASC`;
                } else {
                    sql = `SELECT idDetailProduct, nameDetailProduct, priceDetailProduct FROM detailProduct WHERE idProduct = ${id} ORDER BY priority ASC`;
                }
                return await con.db(sql)
            }

            async function product(id) {
                let sql = await con.db("SELECT idProduct, nameProduct, descProduct, displayPrice FROM product WHERE idCategory = " + id + " ORDER BY priority ASC");
                for (let data of sql) {
                    data.detailProduct = await detailProduct(data.idProduct)
                    data.addOn = await addOn(data.idProduct)
                }
                return sql
            }

            payload = await con.db("SELECT idCategory, nameCategory, descCategory, imageCategory FROM category")

            for (let data of payload) {
                data.product = await product(data.idCategory)
            }

            nodeCache.store(ID, {
                version: req.body.version,
                data: payload
            })
        }

        if (store) {
            if (store.version == req.body.version) {
                payload = store.data
            } else {
                console.log('store already but getData')
                await getData()
            }
        } else {
            console.log('store not found')
            await getData()
        }

        response.success(res, payload)

    } catch (error) {
        console.log(error)
        response.failed(res, error)
    }
}

exports.getDetailProduct = async function (req, res) {
    try {
        const ID = req.params.id;
        const data = await con.db("SELECT A.idProduct, A.nameProduct, A.descProduct, A.displayPrice, B.idDetailProduct, B.nameDetailProduct, B.priceDetailProduct, C.idAddOn, C.nameAddOn, C.priceAddOn FROM product A LEFT JOIN detailProduct B ON B.idProduct = A.idProduct LEFT JOIN addOn C ON C.idDetailProduct = B.idDetailProduct WHERE A.idProduct = " + ID)
        let payload = {
            nameProduct: data[0].nameProduct,
            descProduct: data[0].descProduct,
            displayPrice: data[0].displayPrice,
            detailProduct: []
        }

        for (let row of data) {
            let newRow = payload.detailProduct.filter(function (rows) {
                return rows.idDetailProduct === row.idDetailProduct
            });
            if (newRow.length === 0) {
                payload.detailProduct.push({
                    idDetailProduct: row.idDetailProduct,
                    nameDetailProduct: row.nameDetailProduct,
                    priceDetailProduct: row.priceDetailProduct,
                    addOn: []
                })
            }
        }

        for (let i in payload.detailProduct) {
            let addOn = data.filter(function (rows) {
                return rows.idDetailProduct === payload.detailProduct[i].idDetailProduct
            });

            if (addOn.length > 0) {
                let newAddOn = [];
                for (let row of addOn) {
                    newAddOn.push({
                        idAddOn: row.idAddOn,
                        nameAddOn: row.nameAddOn,
                        priceAddOn: row.priceAddOn
                    })
                }
                payload.detailProduct[i].addOn = newAddOn
            }
        }

        response.success(res, payload)
    } catch (error) {
        console.log(error)
        response.failed(res, error)
    }
}

exports.getOrderByID = async function (req, res) {
    try {
        const ID = req.params.id;
        const sql = await con.db(query.getOrderByID(ID))
        let data = sql[0];
        data.detailLoc = JSON.parse(sql[0].detailLoc)
        response.success(res, data)
    } catch (err) {
        response.failed(res, err)
    }
}

exports.rejectedOrder = async function (req, res) {
    try {
        const ID = req.params.id;
        const idPartner = req.user.idPartner;
        let que = nodeCache.getStore('beauty_' + ID);
        let newQue = que.filter(function (rows) {
            return rows.idPartner != idPartner
        });
        if (newQue.length === 0) {
            await con.db("UPDATE userOrder SET que = 1 WHERE idOrder = " + ID);
        } else {
            nodeCache.store('beauty_' + ID, newQue);
            notifPartner(newQue[0].firebaseToken, ID)
        }
        response.success(res, 'success')
    } catch (err) {
        response.failed(res, err)
    }
}

exports.rejectedOrderForce = async function (req, res) {
    try {
        const idOrder = req.params.id;
        const owner = req.user;
        let data = await con.db(SQL.updateStatus() + "where A.idOrder = " + idOrder);
        if (data.length != 0) {
            const notif = {
                fcmToken: data[0].firebaseToken,
                notif: {
                    title: 'Rejected',
                    subTitle: 'Pesanan Kamu Telah Direject Oleh Admin Jelita Apps, Terimakasih Telah Mencoba Menggunakan Jasa Kami'
                },
                data: {
                    page: 'TabOrder'
                }
            }
            const updateData = await con.db("UPDATE `userOrder` SET `idPartner` = '" + owner.idPartner + "', `idStatus` = '7' WHERE `userOrder`.`idOrder` = " + idOrder);
            if (updateData) {
                response.success(res, updateData)
                await notification.fcmSender(notif)
            } else {
                response.failed(res, updateData)
            }
        } else {
            response.failed(res, '', 'noData')
        }
    } catch (error) {
        console.log(error)
        response.failed(res, error)
    }
}

async function notifPartner(firebaseToken, idOrder) {
    const notif = {
        fcmToken: firebaseToken,
        notif: {
            title: 'Pesanan Baru',
            subTitle: 'Pesanan dengan ID #' + idOrder + ' apakah anda akan menerimanya ?'
        },
        data: {
            page: 'NewOrder',
            ID: idOrder.toString()
        }
    }
    await notification.fcmSender(notif)
}

async function doLoopPartner(ID) {
    try {

        var data = nodeCache.getStore('beauty_' + ID);

        async function getData() {
            if (!nodeCache.getStore('beauty_' + ID)) {
                clearInterval(order)
                await con.db("UPDATE userOrder SET que = 1 WHERE idOrder = " + ID);
                console.log('stop')
            }
            console.log('interval')
            data.shift();
            console.log(data)
            if (data.length === 0) {
                await con.db("UPDATE userOrder SET que = 1 WHERE idOrder = " + ID);
                clearInterval(order)
                console.log('stop')
            } else {
                nodeCache.store('beauty_' + ID, data);
                notifPartner(data[0].firebaseToken, ID)
            }
        }

        console.log('start');
        console.log(data);
        var order = setInterval(getData, 130000);

    } catch (err) {
        response.failed(res, err)
    }
}

async function getPartner(ID, TOTAL, loc) {
    const saldo = TOTAL * 0.1;
    const sql = "SELECT A.*, B.firebaseToken FROM partner A LEFT JOIN token B ON B.idPartner = A.idPartner WHERE A.role = 'partner' AND A.active = '1' AND A.ready = '1' AND A.saldo > " + saldo + " ORDER BY A.totalOrder ASC LIMIT 10";
    let partner = await con.db(sql)
    if (partner.length > 0) {
        for (let row of partner) {
            row.distance = Number(convertLocationIntoRadius({
                latAwal: row.latitude,
                longAwal: row.longitude,
                latAkhir: loc.latitude,
                longAkhir: loc.longitude
            }))
            row.accept = false;
        }
        partner.sort((a, b) => a.totalOrder - b.totalOrder || a.distance - b.distance);
        nodeCache.store('beauty_' + ID, partner);
        notifPartner(partner[0].firebaseToken, ID);
        setTimeout(() => {
            doLoopPartner(ID)
        }, 50000);
    } else {
        await con.db("UPDATE userOrder SET que = 1 WHERE idOrder = " + ID);
    }
}

exports.order = async function (req, res) {
    try {
        const ID = req.user.idUser;
        const loc = req.body.location;
        const data = req.body;
        const TOTAL = req.body.total ? req.body.total : 99999

        async function doOrder(location) {
            
            const createOrder = await con.db("INSERT INTO userOrder (`idOrder`, `idUser`, `idPartner`, `idDetailProduct`, `idAddOn`, `idStatus`, `note`, `detailLoc`, `timeOrder`) VALUES (NULL, '" +
                ID + "', NULL, " +
                data.idDetailProduct + ", " +
                data.idAddOn + ", " +
                1 + ", '" +
                data.note + "', '" +
                location + "', '" +
                moment(data.timeOrder).format('YYYY-MM-DD HH:mm:ss') + "' )")
            if (createOrder) {
                response.success(res, createOrder)
                getPartner(createOrder.insertId, TOTAL, loc)
            } else {
                throw (errorMessage.internalError)
            }
        }

        const getLoc = await con.db("SELECT * FROM location WHERE idUser = " + ID)

        if (getLoc.length !== 0) {
            const updateLoc = await con.db("UPDATE location SET `display` = '" + loc.display +
                "', `latitude` = '" + loc.latitude +
                "', `longitude` = '" + loc.longitude +
                "', `detailLoc` = '" + loc.detailLoc +
                "' WHERE `idLoc` = " + getLoc[0].idLoc)
            if (updateLoc) {
                doOrder(JSON.stringify({
                    display: loc.display,
                    latitude: loc.latitude,
                    longitude: loc.longitude,
                    detailLoc: loc.detailLoc
                }))
            } else {
                throw (errorMessage.internalError)
            }
        } else {
            const createLoc = await con.db("INSERT INTO location (`idLoc`, `idUser`, `display`, `latitude`, `longitude`, `detailLoc`) VALUES (NULL, " +
                ID + ", '" +
                loc.display + "', '" +
                loc.latitude + "', '" +
                loc.longitude + "', '" +
                loc.detailLoc + "')")
            if (createLoc) {
                doOrder(JSON.stringify({
                    display: loc.display,
                    latitude: loc.latitude,
                    longitude: loc.longitude,
                    detailLoc: loc.detailLoc
                }))
            } else {
                console.log('disiniiii')
                throw (errorMessage.internalError)
            }
        }
    } catch (err) {
        response.failed(res, err)
    }
};

exports.updateDescCategory = async function (req, res) {
    try {

        let idCategory = req.body.idCategory;
        let descCategory = req.body.descCategory;
        const updateData = await con.db("UPDATE `category` SET `descCategory` = '" + descCategory + "' WHERE `category`.`idCategory` = " + idCategory)
        if (updateData) {
            response.success(res, updateData)
        } else {
            throw (errorMessage.internalError)
        }

    } catch (err) {
        response.failed(res, err)
    }
};

exports.updateDescProduct = async function (req, res) {
    try {

        let idProduct = req.body.idProduct;
        let descProduct = req.body.descProduct;
        const updateData = await con.db("UPDATE `product` SET `descProduct` = '" + descProduct + "' WHERE `product`.`idProduct` = " + idProduct)
        if (updateData) {
            response.success(res, updateData)
        } else {
            throw (errorMessage.internalError)
        }

    } catch (err) {
        response.failed(res, err)
    }
};

exports.updateDetailPrice = async function (req, res) {
    try {

        let idDetailProduct = req.body.idDetailProduct;
        let priceDetailProduct = req.body.priceDetailProduct;
        const updateData = await con.db("UPDATE `detailProduct` SET `priceDetailProduct` = '" + priceDetailProduct + "' WHERE `detailProduct`.`idDetailProduct` = " + idDetailProduct)
        if (updateData) {
            response.success(res, updateData)
        } else {
            throw (errorMessage.internalError)
        }

    } catch (err) {
        response.failed(res, err)
    }
};

exports.updateAddOnPrice = async function (req, res) {
    try {

        let priceAddOn = req.body.priceAddOn;
        let idAddOn = req.body.idAddOn;
        const updateData = await con.db("UPDATE `addOn` SET `priceAddOn` = '" + priceAddOn + "' WHERE `addOn`.`idAddOn` = " + idAddOn)
        if (updateData) {
            response.success(res, updateData)
        } else {
            throw (errorMessage.internalError)
        }

    } catch (err) {
        response.failed(res, err)
    }
};