require('dotenv').config()
const response = require('../helper');
const con = require('../db');
const AES = require('../helper/AES');

const SQL = "SELECT " +
    "A.idUser, " +
    "A.password, " +
    "A.email, " +
    "A.hp, " +
    "A.userImage, " +
    "A.fullName, " +
    "B.idLoc, " +
    "B.display, " +
    "B.latitude, " +
    "B.longitude, " +
    "B.detailLoc " +
    "FROM user A LEFT JOIN location B ON B.idUser = A.idUser WHERE ";

async function generateToken(payload) {
    const token = AES.jwtSign(payload)
    const getToken = await con.db("SELECT * FROM token WHERE idUser = '" + payload.idUser + "'")
    if (getToken.length == 0) {
        await con.db("INSERT INTO `token` (`idToken`, `idUser`, `idPartner`, `jwtToken`, `firebaseToken`, `deviceID`) VALUES (NULL, " + payload.idUser + ", NULL, '" + token + "', NULL, NULL)")
    }
    else {
        await con.db("UPDATE `token` SET `jwtToken` = '" + token + "' WHERE `token`.`idToken` = " + getToken[0].idToken)
    }
    return token
}

exports.login = async function (req, res) {
    try {
        if (1 + 1 == 2) {
            // if (req.body.pass) {
            const user = req.body.user;
            const pass = AES.decripter(req.body.pass);
            const getData = await con.db(SQL + "email = '" + user + "' OR hp = '" + user + "'")
            if (getData.length != 0) {
                if (AES.decripter(getData[0].password) == pass) {
                    const token = await generateToken(getData[0])
                    response.success(res, token)
                } else {
                    response.failed(res, '', 'wrongPass')
                }
            } else {
                response.failed(res, '', 'authWrong')
            }
        } else {
            response.failed(res, '', 'authWrong')
        }
    } catch (err) {
        response.failed(res, err)
    }
};

exports.loginOwner = async function (req, res) {
    try {
        const user = req.body.username ? req.body.username : 'cs@jelita.co.id';
        const pass = AES.decripter(req.body.pass);
        console.log('*******************', user, pass)
        const getData = await con.db("SELECT A.idToken, A.jwtToken, B.idPartner, B.password, B.email, B.hp, B.partnerImage, B.fullName, B.role, B.saldo, B.active from partner B LEFT JOIN token A ON A.idPartner = B.idPartner WHERE email = '" + user + "' OR hp = '" + user + "'");
        if (getData.length != 0) {
            // if (1 + 1 === 2) {
            if (AES.decripter(getData[0].password) == pass) {
                const token = AES.jwtSignOwner(getData[0])
                if (getData[0].jwtToken) {
                    await con.db("UPDATE `token` SET `jwtToken` = '" + token + "' WHERE `token`.`idToken` = " + getData[0].idToken)
                }
                else {
                    await con.db("INSERT INTO `token` (`idToken`, `idUser`, `idPartner`, `jwtToken`, `firebaseToken`, `deviceID`) VALUES (NULL, NULL, " + getData[0].idPartner + ", '" + token + "', NULL, NULL)")
                }
                response.success(res, token)
            } else {
                response.failed(res, '', 'wrongPass')
            }
        } else {
            response.failed(res, getData, 'deviceUnregistered')
        }
    } catch (err) {
        response.failed(res, err)
    }
};

exports.loginGoogle = async function (req, res) {
    try {

        const google = req.google;
        let data = await con.db(SQL + "email = '" + google.email + "'")

        async function responseLoginGoogle(payload, newUser) {
            const token = await generateToken(payload)
            // if (newUser) {
            //     response.failed(res, token, 'unregistered')
            // } else {
            response.success(res, token)
            // }
        }

        if (data.length !== 0) {
            await responseLoginGoogle(data[0], false)
        } else {
            const createUser = await con.db("INSERT INTO `user` (`idUSer`, `password`, `email`, `hp`, `userImage`, `fullName`) VALUES (NULL, NULL, '" + google.email + "', NULL, '" + google.picture + "', '" + google.name + "')")
            data = await con.db(SQL + "A.idUser = " + createUser.insertId)
            await responseLoginGoogle(data[0], true)
        }
    } catch (err) {
        response.failed(res, err)
    }
};

exports.loginGoogleOwner = async function (req, res) {
    try {

        const google = req.google;
        let getData = await con.db("SELECT A.idToken, A.jwtToken, B.idPartner, B.password, B.email, B.hp, B.partnerImage, B.fullName, B.role, B.saldo, B.active, C.idDetailPartner from partner B LEFT JOIN token A ON A.idPartner = B.idPartner LEFT JOIN detailPartner C ON A.idPartner = C.idPartner WHERE email = '" + google.email + "'");
        if (getData.length != 0) {
            if (getData[0].idDetailPartner) {
                const token = AES.jwtSignOwner(getData[0])
                if (getData[0].jwtToken) {
                    await con.db("UPDATE `token` SET `jwtToken` = '" + token + "' WHERE `token`.`idToken` = " + getData[0].idToken)
                }
                else {
                    await con.db("INSERT INTO `token` (`idToken`, `idUser`, `idPartner`, `jwtToken`, `firebaseToken`, `deviceID`) VALUES (NULL, NULL, " + getData[0].idPartner + ", '" + token + "', NULL, NULL)")
                }
                response.success(res, token)
            } else {
                const token = AES.jwtSignOwner(getData[0])
                if (getData[0].jwtToken) {
                    await con.db("UPDATE `token` SET `jwtToken` = '" + token + "' WHERE `token`.`idToken` = " + getData[0].idToken)
                }
                else {
                    await con.db("INSERT INTO `token` (`idToken`, `idUser`, `idPartner`, `jwtToken`, `firebaseToken`, `deviceID`) VALUES (NULL, NULL, " + getData[0].idPartner + ", '" + token + "', NULL, NULL)")
                }
                response.failed(res, token, 'unregistered')
            }
        } else {
            const createPartner = await con.db("INSERT INTO `partner` (`idPartner`, `password`, `email`, `hp`, `partnerImage`, `fullName`) VALUES (NULL, NULL, '" + google.email + "', NULL, '" + google.picture + "', '" + google.name + "')")
            getData = await con.db("SELECT A.idToken, A.jwtToken, B.idPartner, B.password, B.email, B.hp, B.partnerImage, B.fullName, B.role, B.saldo, B.active from partner B LEFT JOIN token A ON A.idPartner = B.idPartner WHERE B.idPartner = '" + createPartner.insertId + "'");
            const token = AES.jwtSignOwner(getData[0])
            if (getData[0].jwtToken) {
                await con.db("UPDATE `token` SET `jwtToken` = '" + token + "' WHERE `token`.`idToken` = " + getData[0].idToken)
            }
            else {
                await con.db("INSERT INTO `token` (`idToken`, `idUser`, `idPartner`, `jwtToken`, `firebaseToken`, `deviceID`) VALUES (NULL, NULL, " + getData[0].idPartner + ", '" + token + "', NULL, NULL)")
            }
            response.failed(res, token, 'unregistered')
        }
    } catch (err) {
        response.failed(res, err)
    }
};

exports.registerGoogle = async function (req, res) {
    try {

        const hp = req.body.hp;
        const pass = req.body.pass;
        const updateData = await con.db("UPDATE `user` SET `password` = '" + pass + "', `hp` = '" + hp + "' WHERE `user`.`idUser` = " + req.user.idUser)
        if (updateData) {
            const payload = await con.db(SQL + "idUser = " + req.user.idUser)
            const token = await generateToken(payload[0])
            response.success(res, token)
        } else {
            response.failed(res, updateData)
        }

    } catch (err) {
        response.failed(res, err)
    }
};

exports.updateDevice = async function (req, res) {
    try {
        const user = req.user;
        const updateData = await con.db("UPDATE `token` SET `firebaseToken` = '" +
            req.body.firebaseToken + "', `deviceID` = '" +
            req.body.deviceID + "' WHERE `token`.`idUser` = " + user.idUser)
        if (updateData) {
            response.success(res, updateData)
        } else {
            response.failed(res, '')
        }
    } catch (err) {
        response.failed(res, err)
    }
};

exports.updateDeviceOwner = async function (req, res) {
    try {
        const user = req.user;
        const updateData = await con.db("UPDATE `token` SET `firebaseToken` = '" +
            req.body.firebaseToken + "', `deviceID` = '" +
            req.body.deviceID + "' WHERE `token`.`idPartner` = " + user.idPartner)
        if (updateData) {
            response.success(res, updateData)
        } else {
            response.failed(res, '')
        }
    } catch (err) {
        response.failed(res, err)
    }
};

exports.registerDetailPartner = async function (req, res) {
    try {
        if (req.body.name.length < 3) {
            response.failed(res, 'Masukan Nama Lengkap')
        } else if (req.body.alamat.length < 3) {
            response.failed(res, 'Masukan Alamat Lengkap')
        } else if (req.body.hp.length < 3) {
            response.failed(res, 'Masukan No')
        } else if (req.body.nik.length < 3) {
            response.failed(res, 'Masukan NIK Dengan Benar')
        } else if (req.body.kk.length < 3) {
            response.failed(res, 'Masukan KK Dengan Benar')
        } else {
            const user = req.user;
            const info = await con.db(`SELECT * FROM detailPartner where idPartner = ${user.idPartner}`);
            if (info.length > 0) {
                await con.db(`UPDATE detailPartner SET namaLengkap = '${req.body.name}', alamatLengkap = '${req.body.alamat}', namaDarurat = '${req.body.darurat}', hubDarurat = '${req.body.hubDar}', alamatDarurat = '${req.body.alamatDar}', hpDarurat = '${req.body.hpDar}', NIK = '${req.body.nik}', KK = '${req.body.kk}', hp = '${req.body.hp}' WHERE detailPartner.idDetailPartner = ${info[0].idDetailPartner}`)
            } else {
                await con.db("INSERT INTO `detailPartner` (`idDetailPartner`, `idPartner`, `namaLengkap`, `alamatLengkap`, `namaDarurat`, `hubDarurat`, `alamatDarurat`, `hpDarurat`, `NIK`, `KK`, `hp`) VALUES " +
                    "(NULL, " + user.idPartner + ", '" + req.body.name + "', '" + req.body.alamat + "', '" + req.body.darurat + "', '" + req.body.hubDar + "', '" + req.body.alamatDar + "', '" + req.body.hpDar + "', '" + req.body.nik + "', '" + req.body.kk + "', '" + req.body.hp + "')")
            }
            response.success(res, '')
        }
    } catch (err) {
        response.failed(res, err)
    }
};

exports.detailPartner = async function (req, res) {
    try {
        const user = req.user;
        const data = await con.db("SELECT A.idPartner, A.email, B.hp, A.partnerImage, B.namaLengkap fullName, A.role, A.saldo, A.active, A.totalOrder, A.ready, A.latitude, A.longitude, A.dateJoin FROM partner A LEFT JOIN detailPartner B ON B.idPartner = A.idPartner WHERE A.idPartner = " + user.idPartner)
        const rating = await con.db(`Select rating from userOrder WHERE idPartner = ${user.idPartner}`)
        let count = 0;
        for (let row of rating){
            count = row.rating + count;
        }
        let ratingCount = 0;
        if(rating.length > 0){
            ratingCount = count / rating.length
        }
        let newData = {...data[0]};
        newData.ratingCount = ratingCount;
        response.success(res, [newData])
    } catch (err) {
        response.failed(res, err)
    }
};

exports.updateReady = async function (req, res) {
    try {
        const user = req.user;
        const data = await con.db("UPDATE `partner` SET `ready` = '" + req.body.ready + "' WHERE idPartner = " + user.idPartner)
        response.success(res, data)
    } catch (err) {
        response.failed(res, err)
    }
};

exports.updateLocPartner = async function (req, res) {
    try {
        const user = req.user;
        const data = await con.db("UPDATE `partner` SET `latitude` = '" + req.body.latitude + "', `longitude` = '" + req.body.longitude + "' WHERE idPartner = " + user.idPartner)
        response.success(res, data)
    } catch (err) {
        response.failed(res, err)
    }
};

