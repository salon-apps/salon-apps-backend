require('dotenv').config()
const response = require('../helper');
const con = require('../db');
const query = require('../constant/sql');
const notification = require('../helper/notification');
const SQL = require('../constant/sql');
const nodeCache = require('../helper/cache');
const errorMessage = require('../constant/error.json');
const moment = require('moment');
const { convertLocationIntoRadius, compareDistance } = require('../helper/geo');

exports.getHistory = async function (req, res) {
    try {

        const ID = req.user.idUser;
        let data = await con.db(query.history(ID))
        for (let i in data) {
            if (data[i].detailLoc) {
                data[i].detailLoc = JSON.parse(data[i].detailLoc)
            }
        }
        response.success(res, data)

    } catch (error) {
        console.log(error)
        response.failed(res, error)
    }
}

exports.getOrder = async function (req, res) {
    try {

        const latitude = req.body.latitude;
        const longitude = req.body.longitude;

        let data = await con.db(query.getOrderAll())
        for (let i in data) {
            if (data[i].detailLoc) {
                let detailLoc = JSON.parse(data[i].detailLoc);
                data[i].detailLoc = detailLoc;
                data[i].distance = convertLocationIntoRadius({
                    latAwal: detailLoc.latitude,
                    longAwal: detailLoc.longitude,
                    latAkhir: latitude,
                    longAkhir: longitude
                })
            }
        }
        let dataSortDistance = data.sort(compareDistance)
        response.success(res, dataSortDistance)

    } catch (error) {
        console.log(error)
        response.failed(res, error)
    }
}

exports.getHistoryOwner = async function (req, res) {
    try {

        const ID = req.user.idPartner;
        const latitude = req.body.latitude;
        const longitude = req.body.longitude;

        let data = await con.db(query.getHistoryOwner(ID))
        for (let i in data) {
            if (data[i].detailLoc) {
                let detailLoc = JSON.parse(data[i].detailLoc);
                data[i].detailLoc = detailLoc;
                data[i].distance = convertLocationIntoRadius({
                    latAwal: detailLoc.latitude,
                    longAwal: detailLoc.longitude,
                    latAkhir: latitude,
                    longAkhir: longitude
                })
            }
        }
        let dataSortDistance = data.sort(compareDistance)
        response.success(res, dataSortDistance)

    } catch (error) {
        console.log(error)
        response.failed(res, error)
    }
}

exports.accept = async function (req, res) {
    try {
        const idOrder = req.params.id;
        const owner = req.user;
        let data = await con.db(SQL.updateStatus() + "where A.idOrder = " + idOrder);
        if (data.length != 0) {
            const notif = {
                fcmToken: data[0].firebaseToken,
                notif: {
                    title: 'Accepted',
                    subTitle: 'Pesanan dengan ID #' + idOrder + ' telah disetujui oleh mitra kami'
                },
                data: {
                    page: 'TabOrder'
                }
            }
            if (data[0].idStatus == 1) {
                nodeCache.store('beauty_' + idOrder, []);
                const updateData = await con.db("UPDATE `userOrder` SET `idPartner` = '" + owner.idPartner + "', `idStatus` = '2', `que` = '1' WHERE `userOrder`.`idOrder` = " + idOrder);
                if (updateData) {
                    response.success(res, updateData)
                    const dataPartner = await con.db("SELECT * FROM partner WHERE idPartner = " + owner.idPartner);
                    if(dataPartner){
                        await con.db("UPDATE partner SET totalOrder = " + (dataPartner[0].totalOrder + 1) + " WHERE idPartner = " + owner.idPartner);
                    }                    
                    await notification.fcmSender(notif)
                } else {
                    response.failed(res, updateData)
                }
            } else {
                response.failed(res, {
                    d: {
                        data,
                        notif
                    }
                }, 'dataFailed')
            }
        } else {
            response.failed(res, '', 'noData')
        }
    } catch (error) {
        console.log(error)
        response.failed(res, error)
    }
}

exports.processed = async function (req, res) {
    try {
        const idOrder = req.params.id;
        const owner = req.user
        let data = await con.db(SQL.updateStatus() + "where A.idOrder = " + idOrder + " AND A.idPartner = " + owner.idPartner)
        if (data.length != 0) {
            const notif = {
                fcmToken: data[0].firebaseToken,
                notif: {
                    title: 'Prosessed',
                    subTitle: 'Pesanan dengan ID #' + idOrder + ' telah diproses, saat ini mitra sedang perjalanan kerumah mu'
                },
                data: {
                    page: 'TabOrder'
                }
            }
            if (data[0].idStatus == 2) {
                const updateData = await con.db("UPDATE `userOrder` SET `idStatus` = '3' WHERE `userOrder`.`idOrder` = " + idOrder)
                if (updateData) {
                    response.success(res, updateData)
                    await notification.fcmSender(notif)
                } else {
                    response.failed(res, updateData)
                }
            } else {
                response.failed(res, '', 'dataFailed')
            }
        } else {
            response.failed(res, '', 'noData')
        }
    } catch (error) {
        console.log(error)
        response.failed(res, error)
    }
}

exports.worked = async function (req, res) {
    try {
        const owner = req.user;
        const idOrder = req.body.idOrder;
        const latitude = req.body.latitude;
        const longitude = req.body.longitude;


        //This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in km)
        function calcCrow(lat1, lon1, lat2, lon2) {
            var R = 6371; // km
            var dLat = toRad(lat2 - lat1);
            var dLon = toRad(lon2 - lon1);
            var lat1 = toRad(lat1);
            var lat2 = toRad(lat2);

            var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            var d = R * c;
            return d;
        }

        // Converts numeric degrees to radians
        function toRad(Value) {
            return Value * Math.PI / 180;
        }

        let data = await con.db(SQL.updateStatus() + "where A.idOrder = " + idOrder + " AND A.idPartner = " + owner.idPartner)
        if (data.length != 0) {
            const notif = {
                fcmToken: data[0].firebaseToken,
                notif: {
                    title: 'Worked',
                    subTitle: 'Pesanan dengan ID #' + idOrder + ' sedang dikerjakan, selamat menikmati layanan dari kami'
                },
                data: {
                    page: 'TabOrder'
                }
            }
            const location = JSON.parse(data[0].detailLoc);
            if (location.latitude) {
                const distance = calcCrow(latitude, longitude, location.latitude, location.longitude)
                if (distance) {
                    if (distance > 4) {
                        response.failed(res, distance, 'toFarLocation')
                    } else {
                        if (data[0].idStatus == 3) {
                            const updateData = await con.db("UPDATE `userOrder` SET `idStatus` = '4' WHERE `userOrder`.`idOrder` = " + idOrder)
                            if (updateData) {
                                response.success(res, updateData)
                                await notification.fcmSender(notif)
                            } else {
                                response.failed(res, updateData)
                            }
                        } else {
                            response.failed(res, '', 'dataFailed')
                        }
                    }
                } else {
                    response.failed(res, '', 'badRequest')
                }
            } else {
                response.failed(res, '', 'dataFailed')
            }
        } else {
            response.failed(res, '', 'noData')
        }
    } catch (error) {
        console.log(error)
        response.failed(res, error)
    }
}

async function updateSaldo(data){
    const saldo = data.saldo;
    const idPartner = data.idPartner;
    const priceDetailProduct = data.priceDetailProduct;
    const priceAddOn = data.priceAddOn;
    let newSaldo = saldo;
    if(priceAddOn){
        newSaldo = saldo - ((priceDetailProduct + priceAddOn) * 0.2);
    } else {
        newSaldo = saldo - (priceDetailProduct * 0.2);
    }
    await con.db("UPDATE `partner` SET `saldo` = '" + newSaldo + "' WHERE `partner`.`idPartner` = " + idPartner);
}

exports.finish = async function (req, res) {
    try {
        const idOrder = req.params.id;
        const owner = req.user
        let data = await con.db(SQL.updateStatusFinish() + "where A.idOrder = " + idOrder + " AND A.idPartner = " + owner.idPartner)
        if (data.length != 0) {
            const notif = {
                fcmToken: data[0].firebaseToken,
                notif: {
                    title: 'Finish',
                    subTitle: 'Terimakasih telah menggunakan jasa layanan Jelita Homeservices'
                },
                data: {
                    page: 'TabRating',
                    params: idOrder
                }
            }
            if (data[0].idStatus == 4) {
                const updateData = await con.db("UPDATE `userOrder` SET `idStatus` = '5' WHERE `userOrder`.`idOrder` = " + idOrder)
                if (updateData) {
                    updateSaldo(data[0]);
                    response.success(res, updateData)
                    await notification.fcmSender(notif)
                    notification.removeChat(idOrder)
                } else {
                    response.failed(res, updateData)
                }
            } else {
                response.failed(res, '', 'dataFailed')
            }
        } else {
            response.failed(res, '', 'noData')
        }
    } catch (error) {
        console.log(error)
        response.failed(res, error)
    }
}

exports.finishForce = async function (req, res) {
    try {
        const idOrder = req.params.id;
        const owner = req.user
        let data = await con.db(SQL.updateStatusFinish() + "where A.idOrder = " + idOrder + " AND A.idPartner = " + owner.idPartner)
        if (data.length != 0) {
            const notif = {
                fcmToken: data[0].firebaseToken,
                notif: {
                    title: 'Finish',
                    subTitle: 'Terimakasih telah menggunakan jasa layanan Jelita Homeservices'
                },
                data: {
                    page: 'TabOrder'
                }
            }
            const updateData = await con.db("UPDATE `userOrder` SET `idStatus` = '5' WHERE `userOrder`.`idOrder` = " + idOrder)
            if (updateData) {
                updateSaldo(data[0]);
                response.success(res, updateData)
                await notification.fcmSender(notif)
                notification.removeChat(idOrder)
            } else {
                response.failed(res, updateData)
            }
        } else {
            response.failed(res, '', 'noData')
        }
    } catch (error) {
        console.log(error)
        response.failed(res, error)
    }
}

exports.rejected = async function (req, res) {
    try {
        const idOrder = req.params.id;
        const owner = req.user;
        let data = await con.db(SQL.updateStatus() + "where A.idOrder = " + idOrder);
        if (data.length != 0) {
            const notif = {
                fcmToken: data[0].firebaseToken,
                notif: {
                    title: 'Rejected',
                    subTitle: 'Mohon maaf kami sedang tidak dapat menerima pesanan pada jam tersebut, harap mengganti waktu lain'
                },
                data: {
                    page: 'TabOrder'
                }
            }
            if (data[0].idStatus == 1 || data[0].idStatus == 2) {
                const updateData = await con.db("UPDATE `userOrder` SET `idPartner` = '" + owner.idPartner + "', `idStatus` = '7' WHERE `userOrder`.`idOrder` = " + idOrder);
                if (updateData) {
                    response.success(res, updateData)
                    await notification.fcmSender(notif)
                } else {
                    response.failed(res, updateData)
                }
            } else {
                response.failed(res, '', 'dataFailed')
            }
        } else {
            response.failed(res, '', 'noData')
        }
    } catch (error) {
        console.log(error)
        response.failed(res, error)
    }
}

exports.canceled = async function (req, res) {
    try {
        const idOrder = req.params.id;
        const user = req.user
        if (user.idUser) {
            let data = await con.db(SQL.updateStatus() + "where A.idOrder = " + idOrder + " AND A.idUser = " + user.idUser)
            if (data.length != 0) {
                if (data[0].idStatus == 1) {
                    nodeCache.store('beauty_' + idOrder, []);
                    const updateData = await con.db("UPDATE `userOrder` SET `idStatus` = '6' WHERE `userOrder`.`idOrder` = " + idOrder)
                    if (updateData) {
                        response.success(res, updateData)
                    } else {
                        response.failed(res, updateData)
                    }
                } else {
                    response.failed(res, '', 'dataFailed')
                }
            } else {
                response.failed(res, '', 'noData')
            }
        } else {
            response.failed(res, '', 'authFailed')
        }
    } catch (error) {
        console.log(error)
        response.failed(res, error)
    }
}