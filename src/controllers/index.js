require('dotenv').config()
const response = require('../helper');
const AES = require('../helper/AES');
const con = require('../db');
const nodeCache = require('../helper/cache');
const config = require('../constant');
const emailSender = require('../helper/email');
const notifSender = require('../helper/notification');
const moment = require('moment');
const { formatNumber } = require('../helper/formatNumber');

// async function request({
//     method, url, headers, data
// }) {
//     const payload = {
//         headers,
//         method,
//         url,
//         data
//     }

//     const res = await axios(payload);
//     return res.data;
// }

exports.testEmail = async function (req, res) {
    try {

        const email = await emailSender.sender()
        response.success(res, email)

    } catch (error) {
        console.log(error)
        response.failed(res, error)
    }
}

exports.welcome = async function (req, res) {
    try {
        const payload = {
            host: AES.encripter(config.DB().HOST),
            user: AES.encripter(config.DB().DB_USER),
            password: AES.encripter(config.DB().DB_PASS),
            database: AES.encripter(config.DB().DB_NAME),
            connectionLimit: 5
        }
        const data = {
            data: 'Welcome to Home Services API Backend',
            parameter: payload
        }
        response.success(res, data)
    } catch (err) {
        response.failed(res, err)
    }
};

exports.version = async function (req, res, next) {
    try {

        const ID = 'version';
        const store = nodeCache.getStore(ID)
        let data = {
            version: null
        }

        var d = new Date();
        const n = d.getTime();

        async function getData() {
            const sql = await con.db("SELECT version from apiVersion")
            data = sql[0]
            console.log('api version: ' + data.version)
            req.body = data
            nodeCache.store(ID, {
                time: n,
                version: data.version
            })
            next()
        }

        // if (store) {
        if (1 + 1 == 3) {
            if (n - store.time > 1800000) {
                console.log('store already but getData')
                console.log(JSON.stringify(store))
                await getData()
            } else {
                data.version = store.version
                req.body = data
                next()
            }
        } else {
            await getData()
        }

    } catch (error) {
        console.log(error)
        response.failed(res, error)
    }
}

exports.getVersion = async function (req, res) {
    try {

        const version = req.body.version
        response.success(res, version)

    } catch (error) {
        console.log(error)
        response.failed(res, error)
    }
}

exports.testEncript = async function (req, res) {
    try {
        const pass = AES.encripter(req.params.id)
        response.success(res, pass)
    } catch (err) {
        response.failed(res, err)
    }
};

exports.testDecript = async function (req, res) {
    try {
        const pass = AES.decripter(req.body.pass)
        response.success(res, pass)
    } catch (err) {
        response.failed(res, err)
    }
};

exports.testNotif = async function (req, res) {
    try {
        const notif = await notifSender.fcmSender({
            fcmToken: 'cPneZ98ZIhk:APA91bG1vULXMgpV2r1mVuOIhsxEmkDMZGjho501Y87R8xucy1TuBN5lFJrnj66ffO2qg-IFYROAus2lrJDUOhRvW_hWPPDYTE1XpaUDq48HqBhtzKcbQDRI8RqAOErZV3Jx8vSlh38w',
            notif: {
                title: false,
                subTitle: false
            },
            data: {
                param: 'test Notification'
            }
        })
        response.success(res, notif)
    } catch (err) {
        response.failed(res, err)
    }
};

exports.getAbout = async function (req, res) {
    try {
        let data = await con.db(`SELECT * FROM setting`);
        let settings = false
        if (data.length > 0){
            settings = data[0];
            settings.rekening = JSON.parse(settings.rekening);
        }
        const help = {
            desc: 'kita memahami bahwa waktu adalah hal yang sangat berharga. Namun penuhnya kesibukanmu bukan halangan untuk tampil sempurna. Jelita Spa & Salon Home Service kini hadir ditengah padatnya aktifitas kamu. kita siap memberikan layanan salon, barber, spa & reflexology dengan kualitas prima ke tempatmu. Download aplikasi kita, pesan layanan kita, dan kita segera meluncur ke tempat di manapun kamu berada',
            about: [
                {
                    title: 'Keluhan Layanan',
                    child: [
                        {
                            title: 'Status Pesanan',
                            content: 'Untuk melihat status pesanan kamu, terdapat di menu "pesanan ku" yang terletak di tabs bawah setelah home'
                        },
                        {
                            title: 'Status Pesanan Pending Terlalu Lama',
                            content: 'Jika waktu pesanan kamu tidak segera ditanggapi oleh mitra kita, itu terjadi karena permintaan yang sedang tinggi, kita sarankan untuk langsung menanyakan hal tersebut kepada mitra kita, dengan senang hati kita akan cepat membalas pesan kamu lowww ...'
                        },
                        {
                            title: 'Kelebihan Pembayaran',
                            content: 'Kita adalah manusia biasa yang tak luput dengan kesalahan, kita berharap kamu melakukan pengecekan lagi tentang harga yang tertera di aplikasi dan pembayaran kepada mitra agar tiada lagi dusta diantara kita, Kalau mau ngasih tips juga gak papa kog ...'
                        },
                        {
                            title: 'Layanan Tidak Memuaskan',
                            content: 'Yahh kalau terjadi seperti ini langsung hubungi CEO kami aja ya biar langsung ditegur orangnya'
                        }
                    ]
                },
                {
                    title: 'Informasi Layanan',
                    child: [
                        {
                            title: 'Layanan Yang Tersedia',
                            content: 'Saat ini kami menyediakan hampir seluruh layanan salon yang ada seperti pewarnaan rambut, perawatan rambut, smooting, cat kuku, dll'
                        },
                        {
                            title: 'Cara Pembayaran',
                            content: 'Jelita Spa & Salon Home Service saat ini hanya menyediakan pembayaran tunai saja 100% langsung untuk mitra kami :-) '
                        },
                        {
                            title: 'Area Layanan',
                            content: 'Saat ini kami masih berstatus beta dan hanya melayani di ibukota jakarta saja'
                        },
                        {
                            title: 'Cara Melakukan Pemesanan',
                            content: 'Cara memesan layanan kita gampang kog, tinggal masuk kemenu home trus pilih kategori yang kamu mau trus masukin data data kamu. Eiiittt data nya harus bener yaa, kalau salah bisa nangis entar. Abis itu pilih pesan, udah deh ...'
                        },
                        {
                            title: 'Waktu Layanan',
                            content: 'Kami hadir untukmu senin - minggu pukul 09.00 WIB - 23.30 WIB '
                        }
                    ]
                },
                {
                    title: 'Akun & Pembayaran',
                    child: [
                        {
                            title: 'Kerahasiaan Akun Kamu',
                            content: 'Kami berani jamin 100% kerahasiaan akun kamu, aplikasi kita secure kog dienkripsi end to end kayak aplikasi whatshapp'
                        },
                        {
                            title: 'Promo',
                            content: 'Jelita Spa & Salon Home Service saat ini belum menyediakan promo / diskon, tunggu ya bisa kemungkinan besuk ada investor nyuntik dana ke kita, AUTO Diskon dah ...'
                        },
                        {
                            title: 'Pembayaran',
                            content: 'Layanan Jelita Spa & Salon Home Service harganya bersahabat luw... gak jauh beda sama salon salon lainya. Keunggulanya kamu tinggal dirumah aja mencet mencet hp, biar kita yang datang kerumahmu habis itu auto cantik'
                        }
                    ]
                }
            ],
            settings
        }
        response.success(res, help);
    } catch (error) {
        response.failed(res, error)
    }
}