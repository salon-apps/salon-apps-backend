import CryptoJS from "crypto-js";

export const encryptObj = (data) => {
  try {
    let ciphertext = CryptoJS.AES.encrypt(
      JSON.stringify(data),
      "bissmillah"
    ).toString();
    return btoa(ciphertext);
  } catch (error) {
    console.log(error.message);
    return false;
  }
};

export const decryptObj = (data) => {
  try {
    let ciphertext = atob(data);
    let bytes = CryptoJS.AES.decrypt(ciphertext, "bissmillah");
    return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
  } catch (error) {
    console.log(error.message);
    return false;
  }
};