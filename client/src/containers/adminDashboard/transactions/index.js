import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  // Checkbox,
  Button,
  Select,
  MenuItem,
} from "@material-ui/core";
import { CopyToClipboard } from "react-copy-to-clipboard";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import IconButton from "@material-ui/core/IconButton";
import DialogTitle from "@material-ui/core/DialogTitle";
import AppBar from "@material-ui/core/AppBar";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import Toolbar from "@material-ui/core/Toolbar";
import CloseIcon from "@material-ui/icons/Close";
import Grid from "@material-ui/core/Grid";
import api from "api/admin";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import VisibilityIcon from "@material-ui/icons/Visibility";
import { formatNumber } from "utils/parse";
import moment from "moment";
import * as FileSaver from "file-saver";
import * as XLSX from "xlsx";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const fileType =
  "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
const fileExtension = ".xlsx";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
  table: {
    minWidth: 650,
  },
}));

export default function TransactionsTable() {
  const classes = useStyles();

  const [listData, setListData] = useState([]);
  // const [checked, setChecked] = useState({});
  const [snackbar, setSnackbar] = useState({
    open: false,
    message: "",
  });
  const [detail, setDetail] = useState({
    open: false,
    payload: null,
  });
  const [resi, setResi] = useState({
    open: false,
    value: "",
    id: null,
  });

  const exportToExcel = () => {
    let payload = [];

    for (let row of listData) {
      payload.push({
        "Order Id": row.id_transaksi,
        "Waktu Transaksi": moment(row.create_at).format("DD/MM/YYYY hh:mm:ss"),
        "Nama Produk": row.products[0].nama_produk,
        Nama: row.alamat_lengkap[0].nama_penerima,
        Email: row.alamat_lengkap[0].email,
        "No Whatsapp": row.alamat_lengkap[0].no_whatsapp,
        "Alamat Lengkap": row.alamat_lengkap[0].alamat_lengkap,
        Provinsi: row.alamat_lengkap[0].provinsi,
        "Kota/Kabupaten": row.alamat_lengkap[0].kota,
        Kecamatan: row.alamat_lengkap[0].kecamatan,
        "Kode Pos": row.alamat_lengkap[0].kode_pos,
        "Status Paket": "NON COD",
        "Status Pembayaran": row.status_pembayaran,
        "Status Transaksi": row.status_transaksi,
        "Metode Pembayaran": row.payment.title,
        "Harga Produk": formatNumber(row.total_harga),
        "Jumlah Produk": row.jumlah,
        Notes: "-",
        Kurir: `${row.kurir_code} - ${row.kurir_service}`,
        "Ongkos Kirim": formatNumber(row.total_pengiriman),
        "Nomor Resi": row.no_resi ? row.no_resi : "-",
        "Total Pembayaran": formatNumber(row.total_pembayaran),
      });
    }

    let wscols = [{ wch: 5 }];

    let ws = XLSX.utils.json_to_sheet(payload);

    ws["!cols"] = wscols;

    let wb = {
      Sheets: {
        data: ws,
      },
      SheetNames: ["data"],
    };
    let excelBuffer = XLSX.write(wb, { bookType: "xlsx", type: "array" });
    let data = new Blob([excelBuffer], { type: fileType });
    FileSaver.saveAs(
      data,
      `superkeren ${moment(new Date()).format("DD_MM_YYYY")}${fileExtension}`
    );
  };

  const updatePayment = async (row, status) => {
    try {
      let response = await api.updatePaymentStatus({
        id_transaksi: row.id_transaksi,
        status,
      });
      if (response.success) {
        getData();
      }
    } catch (error) {
      console.log(error);
    }
  };

  const updateStatus = async (row, status) => {
    try {
      let response = await api.updateTransactionsStatus({
        id_transaksi: row.id_transaksi,
        status,
      });
      if (response.success) {
        getData();
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getData = async () => {
    try {
      let response = await api.listTransactions();
      setListData(response.payload);
    } catch (error) {
      console.log(error);
    }
  };

  const renderProductName = (payload) => {
    try {
      let newData = "";
      for (let row of payload){
        if (newData.length === 0){
          newData += row.nama_produk
        } else {
          newData += `, ${row.nama_produk}`
        }
      }
      return newData;
    } catch (error) {
      console.log(error);
      return "";
    }
  };

  const updateResi = async () => {
    try {
      let response = await api.updateTransactionsStatusResi({
        id_transaksi: resi.id,
        resi: resi.value,
      });
      if (response) {
        setResi({
          open: false,
          value: "",
          id: null,
        });
        getData();
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleClose = () => {
    setResi({
      open: false,
      value: "",
      id: null,
    });
  };

  const viewRow = (row) => {
    let newData = [
      {
        key: "Order Id",
        value: row.id_transaksi,
      },
      {
        key: "Waktu Transaksi",
        value: moment(row.create_at).format("DD/MM/YYYY hh:mm:ss"),
      },
      {
        key: "Nama Penerima",
        value: row.alamat_lengkap[0].nama_penerima,
      },
      {
        key: "Email",
        value: row.alamat_lengkap[0].email,
      },
      {
        key: "No Whatsapp",
        value: row.alamat_lengkap[0].no_whatsapp,
      },
      {
        key: "Alamat Lengkap",
        value: row.alamat_lengkap[0].alamat_lengkap,
      },
      {
        key: "Provinsi",
        value: row.alamat_lengkap[0].provinsi,
      },
      {
        key: "Kota/Kabupaten",
        value: row.alamat_lengkap[0].kota,
      },
      {
        key: "Kecamatan",
        value: row.alamat_lengkap[0].kota,
      },
      {
        key: "Kode Pos",
        value: row.alamat_lengkap[0].kode_pos,
      },
      {
        key: "Status Paket",
        value: "NON COD",
      },
      {
        key: "Status Pembayaran",
        value: row.status_pembayaran,
      },
      {
        key: "Status Transaksi",
        value: row.status_transaksi,
      },
      {
        key: "Metode Pembayaran",
        value: row.payment.title,
      },
      {
        key: "Nama Produk",
        value: renderProductName(row.products),
      },
      {
        key: "Total Harga Produk",
        value: formatNumber(row.total_harga),
      },
      {
        key: "Jumlah Produk",
        value: row.jumlah,
      },
      {
        key: "Notes",
        value: "-",
      },
      {
        key: "Kurir",
        value: `${row.kurir_code} - ${row.kurir_service}`,
      },
      {
        key: "Ongkos Kirim",
        value: formatNumber(row.total_pengiriman),
      },
      {
        key: "Nomor Resi",
        value: row.no_resi ? row.no_resi : "-",
      },
      {
        key: "Total Pembayaran",
        value: formatNumber(row.total_pembayaran),
      },
    ];
    setDetail({
      open: true,
      payload: newData,
    });
  };

  const handleCloseSnackbar = () => {
    setSnackbar({ ...snackbar, open: false });
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <>
      <Snackbar
        anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
        open={snackbar.open}
        autoHideDuration={2000}
        onClose={handleCloseSnackbar}
      >
        <Alert onClose={handleCloseSnackbar} severity="success">
          {snackbar.message}
        </Alert>
      </Snackbar>
      <Dialog
        fullScreen
        open={detail.open}
        onClose={() => {
          setDetail({ ...detail, open: false });
        }}
      >
        <AppBar>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={() => {
                setDetail({ ...detail, open: false });
              }}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <div
          style={{
            marginBottom: 50,
          }}
        />
        <Divider />
        <Grid
          container
          spacing={3}
          style={{
            padding: 50,
          }}
        >
          {detail.payload
            ? detail.payload.map((row, i) => (
                <Grid item xs={3} key={i.toString()}>
                  <CopyToClipboard
                    text={row.value}
                    onCopy={() => {
                      setSnackbar({
                        open: true,
                        message: "Success Copy",
                      });
                    }}
                  >
                    <List>
                      <ListItem button>
                        <ListItemText primary={row.key} secondary={row.value} />
                      </ListItem>
                    </List>
                  </CopyToClipboard>
                </Grid>
              ))
            : null}
        </Grid>
      </Dialog>
      <Dialog
        open={resi.open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">No. Resi</DialogTitle>
        <DialogContent>
          <DialogContentText>
            harap Input No. Resi dengan sebenar benarnya untuk pemesanan ini
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            label="Input Resi"
            value={resi.value}
            onChange={(e) => {
              setResi({ ...resi, value: e.target.value });
            }}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button
            onClick={() => {
              updateResi();
            }}
            color="primary"
          >
            Save
          </Button>
        </DialogActions>
      </Dialog>
      <Button
        variant="contained"
        color="primary"
        onClick={() => {
          exportToExcel();
        }}
      >
        Export Excel
      </Button>
      <TableContainer
        component={Paper}
        style={{
          marginTop: 10,
        }}
      >
        <Table
          className={classes.table}
          aria-label="simple table"
          size="small"
          stickyHeader
        >
          <TableHead>
            <TableRow>
              <TableCell>#</TableCell>
              <TableCell>Order Id</TableCell>
              <TableCell>Waktu Transaksi</TableCell>
              <TableCell>Nama Produk</TableCell>
              <TableCell>Nama</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>No Whatsapp</TableCell>
              <TableCell>Alamat Lengkap</TableCell>
              <TableCell>Provinsi</TableCell>
              <TableCell>Kota/Kabupaten</TableCell>
              <TableCell>Kecamatan</TableCell>
              <TableCell>Kode Pos</TableCell>
              <TableCell>Status Paket</TableCell>
              <TableCell>Status Pembayaran</TableCell>
              <TableCell>Status Transaksi</TableCell>
              <TableCell>Metode Pembayaran</TableCell>
              <TableCell>Total Harga Produk</TableCell>
              <TableCell>Jumlah Produk</TableCell>
              <TableCell>Notes</TableCell>
              <TableCell>Kurir</TableCell>
              <TableCell>Ongkos Kirim</TableCell>
              <TableCell>Nomor Resi</TableCell>
              <TableCell>Total Pembayaran</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {listData.map((row, i) => (
              <TableRow key={i.toString()}>
                <TableCell>
                  {/* <Checkbox
                    value={checked[i]}
                    onChange={() => {
                      let newData = { ...checked };
                      newData[i] = !newData[i];
                      setChecked(newData);
                    }}
                    inputProps={{ "aria-label": "primary checkbox" }}
                  /> */}
                  <IconButton
                    onClick={() => {
                      viewRow(row);
                    }}
                    color="primary"
                    aria-label="upload picture"
                    component="span"
                  >
                    <VisibilityIcon />
                  </IconButton>
                </TableCell>
                <TableCell>{row.id_transaksi}</TableCell>
                <TableCell>
                  {moment(row.create_at).format("DD/MM/YYYY hh:mm:ss")}
                </TableCell>
                <TableCell>{renderProductName(row.products)}</TableCell>
                <TableCell>{row.alamat_lengkap[0].nama_penerima}</TableCell>
                <TableCell>{row.alamat_lengkap[0].email}</TableCell>
                <TableCell>{row.alamat_lengkap[0].no_whatsapp}</TableCell>
                <TableCell>{row.alamat_lengkap[0].alamat_lengkap}</TableCell>
                <TableCell>{row.alamat_lengkap[0].provinsi}</TableCell>
                <TableCell>{row.alamat_lengkap[0].kota}</TableCell>
                <TableCell>{row.alamat_lengkap[0].kecamatan}</TableCell>
                <TableCell>{row.alamat_lengkap[0].kode_pos}</TableCell>
                <TableCell>NON COD</TableCell>
                <TableCell>
                  <Select
                    value={row.status_pembayaran}
                    onChange={(e) => {
                      updatePayment(row, e.target.value);
                    }}
                  >
                    <MenuItem value={"unpaid"}>Belum Dibayar</MenuItem>
                    <MenuItem value={"paid"}>Dibayar</MenuItem>
                  </Select>
                </TableCell>
                <TableCell>
                  <Select
                    value={row.status_transaksi}
                    onChange={(e) => {
                      updateStatus(row, e.target.value);
                    }}
                  >
                    <MenuItem value={"pending"}>Pending</MenuItem>
                    <MenuItem value={"processing"}>Diproses</MenuItem>
                    <MenuItem value={"completed"}>Selesai</MenuItem>
                    <MenuItem value={"cancelled"}>Dibatalkan</MenuItem>
                    <MenuItem value={"refunded"}>Dikembalikan</MenuItem>
                  </Select>
                </TableCell>
                <TableCell>{row.payment.title}</TableCell>
                <TableCell>{formatNumber(row.total_harga)}</TableCell>
                <TableCell>{row.jumlah}</TableCell>
                <TableCell>-</TableCell>
                <TableCell>
                  {row.kurir_code} - {row.kurir_service}
                </TableCell>
                <TableCell>{formatNumber(row.total_pengiriman)}</TableCell>
                <TableCell>
                  {row.no_resi ? (
                    <Button
                      size={"small"}
                      variant="contained"
                      color="primary"
                      onClick={() => {
                        setResi({
                          open: true,
                          value: row.no_resi ? row.no_resi : "",
                          id: row.id_transaksi,
                        });
                      }}
                    >
                      {row.no_resi}
                    </Button>
                  ) : (
                    <Button
                      size={"small"}
                      variant="contained"
                      color="primary"
                      onClick={() => {
                        setResi({
                          open: true,
                          value: row.no_resi ? row.no_resi : "",
                          id: row.id_transaksi,
                        });
                      }}
                    >
                      Input Nomor Resi
                    </Button>
                  )}
                </TableCell>
                <TableCell>{formatNumber(row.total_pembayaran)}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
}
