import React from "react";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { colors } from "@material-ui/core";

import BgImage from "assets/bg_crm.png";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://woo-wa.com/">
        Woowa
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
  },
  image: {
    backgroundImage: `url(${BgImage})`,
    backgroundRepeat: "no-repeat",
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[50]
        : theme.palette.grey[900],
    backgroundSize: "cover",
    backgroundPosition: "center",
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignInSide() {
  const classes = useStyles();

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid
        item
        xs={12}
        sm={8}
        md={5}
        component={Paper}
        elevation={6}
        square
        style={{
          position: "relative",
        }}
      >
        <div
          className={classes.paper}
          style={{
            justifyContent: "space-between",
            height: '80%'
          }}
        >
          <div>
            <LockOutlinedIcon
                color={'secondary'}
              style={{
                fontSize: 100,
                border: '4px solid #f50057',
                borderRadius: 100,
                padding: 10
              }}
            />
            <div
              style={{
                height: 50,
              }}
            />
            <Typography component="h1" variant="h5">
              Daftarkan Google Email Saya
            </Typography>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              helperText={
                "Email Anda Belum Terdaftar Untuk Fitur Baru Ini, silakan daftarkan email google anda untuk mencoba fitur ini"
              }
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
            />
          </div>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Daftar
          </Button>
        </div>
        <Box
          mt={5}
          style={{
            position: "absolute",
            bottom: 10,
            width: "100%",
          }}
        >
          <Copyright />
        </Box>
      </Grid>
    </Grid>
  );
}
