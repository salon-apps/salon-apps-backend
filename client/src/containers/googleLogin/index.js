import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { circularProgressBarAction } from "redux/actions";
import Authentications from "./authentications";
import { decryptObj } from "utils/crypto";
import { getQueryParamsByName } from "utils/queryParams";
import api from "api/auth";
// import { colors } from '@material-ui/core';

const GoogleLogin = () => {
  const dispatch = useDispatch();
  const [userInfo, setUserInfo] = useState(false);

//   http://localhost:3000/node/client?token=VTJGc2RHVmtYMTlBcDlOclBsQnI1blBqdFFDa29LY2k3eFdTcFR4QlVLNEloMHlCQnRkTDRJamc2VlBRVisxZ3NCd3h0ekZHQ1lkY3E2eklIVXdFK3EzWElKK0sweHdBVlkxc0ZkYzJ3NmNxMW95L0pGcEE3bURrd1VSeGZsMi9wWDBoWS9pRm81bTJvNzRCTGY2UVRYNld1SFVsOW9Tb2tnc1NQVGFCY2hCeGFJZGRGOTVzZE44UjhNZklMZjQy
  useEffect(() => {
    async function fetch() {
      try {
        dispatch(circularProgressBarAction(false));
        let token = getQueryParamsByName("token");
        if (token) {
          let decryptedData = decryptObj(token);
          if (decryptedData && decryptedData.email && decryptedData.key){
            window.localStorage.setItem('userInfo', JSON.stringify(decryptedData));
            let response = await api.googleMe(token);
            console.log('reponse', response);
            if (response.success){
              setUserInfo(response.data);
            } else {
              setUserInfo('UnAuthorized')
            }
          } 
        }
      } catch (error) {
        console.log(error.message);
      }
    }
    fetch();
  }, [dispatch]);

  useEffect(() => {
    console.log('userInfom', userInfo);
  }, [userInfo]);

  return (
    <Authentications userInfo={userInfo} />
  );
};

export default GoogleLogin;
