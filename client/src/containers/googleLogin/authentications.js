import React from "react";
import { useDispatch } from "react-redux";
import { circularProgressBarAction } from "redux/actions";
import {
  Button,
  CssBaseline,
  Link,
  Paper,
  Box,
  Grid,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { colors } from "@material-ui/core";

import BgImage from "resource/image/bg_crm.png";
import LogoImage from "resource/image/crm.png";
import api from "api/auth";
import { getSafe } from "utils/safe";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://woo-wa.com/">
        Woowa
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
  },
  image: {
    backgroundImage: `url(${BgImage})`,
    backgroundRepeat: "no-repeat",
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[50]
        : theme.palette.grey[900],
    backgroundSize: "cover",
    backgroundPosition: "center",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center"
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Authentications = ({ userInfo }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const doLogin = async () => {
    try {
      dispatch(circularProgressBarAction(true));
      let response = await api.googleLogin();
      if (response) {
        dispatch(circularProgressBarAction(true));
        if (response.data) {
          window.location.replace(response.data);
        }
      }
    } catch (error) {
      dispatch(circularProgressBarAction(false));
      console.log(error);
    }
  };

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image}>
        <img src={LogoImage} alt='logo' style={{
          width: '50vh',
          height: '50vh',
          backgroundColor: colors.lightBlue['50'],
          borderRadius: '100%',
          boxShadow: '0 0 7px 0px #165AA6'
        }} />
      </Grid>
      <Grid
        item
        xs={12}
        sm={8}
        md={5}
        component={Paper}
        elevation={6}
        square
        style={{
          position: "relative",
        }}
      >
        <div
          className={classes.paper}
          style={{
            justifyContent: "space-between",
            height: "80%",
          }}
        >
          <div>
            <img
              src={getSafe(
                () => userInfo.photos[0].url,
                "https://developers.google.com/identity/images/g-logo.png"
              )}
              style={{
                width: 100,
                height: 100,
                border: "4px solid #f50057",
                borderRadius: 100,
                padding: 10,
              }}
              alt={""}
            />
            <div
              style={{
                height: 50,
              }}
            />
            {getSafe(() => userInfo.names[0].displayName, false) ? (
              <>
              <Typography component="h1" variant="h5">
                {`Selamat Datang "${userInfo.names[0].displayName}"`}
              </Typography>
              <Typography component="p" variant="subtitle2">
                {`Email Anda ${userInfo.emailAddresses[0].value} telah terautentikasi`}
              </Typography>
              </>
            ) : (
              <Typography component="h1" variant="h5">
                Integrasikan dengan akun google anda
              </Typography>
            )}
          </div>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            disabled={userInfo !== "UnAuthorized"}
            onClick={() => {
              doLogin();
            }}
          >
            {userInfo
              ? userInfo === "UnAuthorized"
                ? "Mulai Sekarang"
                : "Telah Terautentikasi"
              : "ID Anda Tidak Ditemukan"}
          </Button>
        </div>
        <Box
          mt={5}
          style={{
            position: "absolute",
            bottom: 10,
            width: "100%",
          }}
        >
          <Copyright />
        </Box>
      </Grid>
    </Grid>
  );
};

export default Authentications;
