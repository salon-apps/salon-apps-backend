import React, { useEffect, useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function AdminLogIn() {
  const classes = useStyles();
  const [login, setLogin] = useState({
    user: "",
    pass: "",
  });

  const doLogin = () => {
    if (login.user === 'admin' && login.pass === 'admin'){
        localStorage.setItem('token', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZF91c2VyIjo3LCJlbWFpbCI6ImFkbWluQHN1cGVya2VyZW4uY29tIiwicGFzc3dvcmQiOiJVMkZzZEdWa1gxOTlBM2dBUDRnNkZFZHJNaW9qeVZCN0wwcXZseDF2aHVjPSIsIm5vX3doYXRzYXBwIjoiNjI4OTk5MDY0ODA4IiwiZm90b19wcm9maWwiOm51bGwsImlhdCI6MTYzMDIwNTc0Nn0.aLc1UOWP9MDUeyyxUOhycmjR2BRk4727Q2-AmXdqd5A');
        window.location.reload();
    } else {
        alert('wrong username / password');
    }
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            value={login.user}
            onChange={(e) => {
                setLogin({...login, user: e.target.value})
            }}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            value={login.pass}
            onKeyPress={(e)=> {
                if (e.key === 'Enter'){
                    doLogin();
                }
            }}
            onChange={(e)=> {
                setLogin({...login, pass: e.target.value})
            }}
          />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={()=> {
                doLogin();
            }}
          >
            Sign In
          </Button>
      </div>
      <Box mt={8}>
        <Typography variant="body2" color="textSecondary" align="center">
          {"Copyright © "}
          <Link color="inherit" href="https://material-ui.com/">
            Superkeren
          </Link>{" "}
          {new Date().getFullYear()}
          {"."}
        </Typography>
      </Box>
    </Container>
  );
}
