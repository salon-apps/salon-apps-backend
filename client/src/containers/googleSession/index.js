import { apiToken } from "api";
import api from "api/auth";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { circularProgressBarAction } from "redux/actions";
import { getQueryParamsByName } from "utils/queryParams";
import { getSafe } from "utils/safe";
const isDev = process.env.NODE_ENV !== "production";

const GoogleStoreSession = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    async function fetch() {
      try {
        let token = getQueryParamsByName("token");
        let code = getQueryParamsByName("code");
        let data = getSafe(
          () => JSON.parse(window.localStorage.getItem("userInfo")),
          false
        );
        if (data) {
          if (token && code) {
            dispatch(circularProgressBarAction(true));
            let response = await api.googleStoreSession({
              woowa_email: data.email,
              woowa_key: data.key,
              code,
              token,
            });
            if (response) {
              dispatch(circularProgressBarAction(false));
              if (response.success) {
                let newToken = apiToken(response.data.woowa_email);
                let link = `https://crm.woo-wa.com/node/client?token=${newToken}`;
                if (isDev) {
                  link = `http://localhost:3000/node/client?token=${newToken}`;
                }
                window.location.replace(link);
              }
            }
          }
        }
      } catch (error) {
        dispatch(circularProgressBarAction(false));
        console.log(error.message);
      }
    }
    fetch();
  }, [dispatch]);

  return <div />;
};

export default GoogleStoreSession;
