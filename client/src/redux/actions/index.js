import * as types from "../constant" ;

/*
 * Set global progress.isLoading to true
 */
export const circularProgressBarAction = (params) => {
  return {
    type: types.SET_CIRCULAR_PROGRESS_BAR,
    params,
  };
};