import { combineReducers } from 'redux';
import auth from './auth/reducer';
import view from './view';

const rootReducer = combineReducers({ 
    auth, 
    view
})

export default rootReducer