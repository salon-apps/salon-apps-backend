import { SET_CIRCULAR_PROGRESS_BAR } from '../../constant'

const initState = {
    circularProgressBar: false,
    linierProgressBar: false,
}
 
const reducer = (state = initState, action) => {
    switch (action.type) {
        case SET_CIRCULAR_PROGRESS_BAR:
            return {
                ...state,
                circularProgressBar: action.params,
            }
        default:
            return state
     }
}
 
export default reducer