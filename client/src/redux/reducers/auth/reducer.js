import { SIGNIN } from '../../constant'

const initState = {
    user: false,
}
 
const reducer = (state = initState, action) => {
    switch (action.type) {
        case SIGNIN:
            
            return {
                ...state,
                user: action.data,
            }
        default:
            return state
     }
}
 
export default reducer