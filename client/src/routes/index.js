import React from "react";
import { useSelector } from "react-redux";
import { Helmet } from "react-helmet";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import AdminLogIn from "containers/adminLogin";
import AdminDashboard from "containers/adminDashboard";

const { appTitle, appDescription } = {
  appTitle: "Superkeren Dashboard",
  appDescription: "This website is dashboard from Superkeren",
};

const BASE_URL = '/superkeren/admin'

const MainRoute = () => {
  const { circularProgressBar } = useSelector((state) => state.view);
  const token = localStorage.getItem('token');

  return (
    <div className="App">
      <Helmet titleTemplate={`%s - ${appTitle}`} defaultTitle={appTitle}>
        <meta name="description" content={appDescription} />
      </Helmet>
      <BrowserRouter>
        <Switch>
          <Route
            exact
            path={BASE_URL}
            name="Login"
            render={(props) => token ? <AdminDashboard {...props}/> : <AdminLogIn {...props} />}
          />
          <Route
            path={`${BASE_URL}/404`}
            component={() => {
              return (
                <div className={"flex-center-row full"}>
                  <p>page not found</p>
                </div>
              );
            }}
          />
          <Redirect to={`${BASE_URL}/404`} />
        </Switch>
      </BrowserRouter>
      {circularProgressBar ? (
        <>
          <div className={"preload-container"} />
          <div className={"preload-init"}>
            <div className="lds-roller">
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
            </div>
          </div>
        </>
      ) : null}
    </div>
  );
};

export default MainRoute;
