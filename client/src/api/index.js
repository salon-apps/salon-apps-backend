import axios from 'axios'
// import { encryptObj } from 'utils/crypto';
const isDev = process.env.NODE_ENV !== 'production';
// const isDev = false;

const HOST = isDev ? 'http://localhost:3012/superkeren' : 'http://159.65.131.78:3000/superkeren';

const token = localStorage.getItem('token')

export const baseApi = async ({
    method,
    url,
    data
}) => {
   
    let newMethod = 'get';
    let newData = null;

    let newUrl = `${HOST}/${url}`;

    if (method) {
        newMethod = method;
    }
    if (data) {
        newData = data;
    }

    let headers = {
        'Content-Type': 'application/json',
    };

    if (token){
        headers['Authorization'] = `SuPerKeren ${token}`
    }

    let payload = {
        headers,
        method: newMethod,
        url: newUrl,
        data: newData,
    }
    let response = await axios(payload);
    return response.data
};