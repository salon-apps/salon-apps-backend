import { baseApi } from ".";

const api = {};

api.googleLogin = async () => {
  let response = await baseApi({
    url: `google/login`,
  });
  return response;
};

api.googleStoreSession = async (data) => {
  let response = await baseApi({
    url: `google/store-session`,
    method: "post",
    data,
  });
  return response;
};

api.googleMe = async (token) => {
  let response = await baseApi({
    url: `google/me`,
    token
  });
  return response;
};

export default api;
