import { baseApi } from ".";

const api = {};

api.listTransactions = async () => {
  let response = await baseApi({
    url: `transactions`,
  });
  return response;
};

api.updatePaymentStatus = async (data) => {
  let response = await baseApi({
    url: "payment/update-payment",
    method: 'put',
    data
  });
  return response;
};

api.updateTransactionsStatus = async (data) => {
  let response = await baseApi({
    url: "transactions/update-status",
    method: 'put',
    data
  });
  return response;
};

api.updateTransactionsStatusResi = async (data) => {
  let response = await baseApi({
    url: "transactions/update-resi",
    method: 'put',
    data
  });
  return response;
};

export default api;
