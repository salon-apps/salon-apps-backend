require('dotenv').config();
const express = require('express');
const cors = require('cors')
const path = require('path');
const multer = require('multer');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const routes = require("./src/routes");
const superkerenRoute = require('./src/routes/superkeren');
const response = require('./src/helper/index');
const serveIndex = require('serve-index')

const app = express();

var storage = multer.diskStorage({
  destination: (req, file, cb) => {
      cb(null, './public/jelita')
  },
  filename: (req, file, cb) => {
      cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
  }
});

//will be using this for uplading
const upload = multer({ storage: storage });

app.use(cors())
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// for view file public images uploaded
app.use(express.static(path.join(__dirname, 'public')));
app.use('/assets', express.static('public/jelita'), serveIndex('public/jelita', { 'icons': true }));

// Serve any static files
app.use('/superkeren/admin', express.static(path.join(__dirname, 'client/build')))
// Handle React routing, return all requests to React app
app.get('/superkeren/admin/*', function (_req, res) {
  res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
});

routes(app, upload);
// app.use('/superkeren/', superkerenRoute);

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// catch 404 and forward to error handler
app.use((req, res) => {
  response.failed2(res, `${req.originalUrl} not found`)
});

module.exports = app;
